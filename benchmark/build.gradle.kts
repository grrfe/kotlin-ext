plugins {
    kotlin("jvm")
    id("org.jetbrains.kotlinx.benchmark")
    kotlin("plugin.allopen")
}

allOpen {
    annotation("org.openjdk.jmh.annotations.State")
}

benchmark {
    targets.register("main")
}

dependencies {
    implementation(project(":uri"))
    implementation("org.jetbrains.kotlinx:kotlinx-benchmark-runtime:_")
}
