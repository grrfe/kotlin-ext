package fe.std.uri

import kotlinx.benchmark.BenchmarkTimeUnit
import kotlinx.benchmark.Blackhole
import org.openjdk.jmh.annotations.Benchmark
import org.openjdk.jmh.annotations.BenchmarkMode
import org.openjdk.jmh.annotations.Measurement
import org.openjdk.jmh.annotations.Mode
import org.openjdk.jmh.annotations.OutputTimeUnit
import org.openjdk.jmh.annotations.Scope
import org.openjdk.jmh.annotations.Setup
import org.openjdk.jmh.annotations.State
import org.openjdk.jmh.annotations.TearDown
import org.openjdk.jmh.annotations.Warmup

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(BenchmarkTimeUnit.NANOSECONDS)
@Warmup(iterations = 5, time = 500, timeUnit = BenchmarkTimeUnit.MILLISECONDS)
@Measurement(iterations = 100, time = 5, timeUnit = BenchmarkTimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
public class FixUriBenchmark {
    private lateinit var tests: MutableList<String>

    @Setup
    public fun prepare() {
        tests = mutableListOf(
            "",
            ":",
            "http://",
            "http:/",
            "file:google.com",
            "google.com",
            "http://google.com",
            "https://google.com"
        )
    }

    @TearDown
    public fun cleanup() {
        tests.clear()
    }

    @Benchmark
    public fun benchmarkFast(bh: Blackhole) {
        for (test in tests) {
            bh.consume(UrlFactory.fixHttpUrlFast(test))
        }
    }

    @Benchmark
    public fun benchmarkSlow(bh: Blackhole) {
        for (test in tests) {
            bh.consume(UrlFactory.fixHttpUrl(test))
        }
    }
}
