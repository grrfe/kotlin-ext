import fe.buildlogic.Version
import fe.buildlogic.extension.asProvider
import fe.buildlogic.extension.getReleaseVersion
import fe.buildlogic.publishing.PublicationComponent
import fe.buildlogic.publishing.publish
import net.nemerosa.versioning.VersioningExtension
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmProjectExtension

plugins {
    kotlin("jvm") apply false
    `maven-publish`
    id("net.nemerosa.versioning") apply false
    id("com.gitlab.grrfe.build-logic-plugin")
}

val baseGroup = "com.gitlab.grrfe.kotlin-ext"
val multiplatformProjects = arrayOf("io-kotlinx")

subprojects {
    val isPlatform = name == "platform"
    val isTest = name.endsWith("testing")

    val isJvm = !multiplatformProjects.any { name.startsWith(it) }

    apply(plugin = "net.nemerosa.versioning")
    apply(plugin = "org.gradle.maven-publish")
    apply(plugin = "com.gitlab.grrfe.build-logic-plugin")

    if (!isPlatform) {
        if (isJvm) {
            apply(plugin = "org.jetbrains.kotlin.jvm")
        }

        repositories {
            mavenCentral()
            maven(url = "https://jitpack.io")
        }
    }

    val versionProvider = with(extensions["versioning"] as VersioningExtension) {
        asProvider(this@subprojects)
    }

    group = baseGroup
    version = versionProvider.getReleaseVersion()

    if (!isPlatform && isJvm) {
        with(extensions["kotlin"] as KotlinJvmProjectExtension) {
            jvmToolchain(Version.JVM)
            if (!isTest) {
                explicitApi()
            }
            compilerOptions.freeCompilerArgs.add("-Xwhen-guards")
        }

        with(extensions["java"] as JavaPluginExtension) {
            withJavadocJar()
            withSourcesJar()
        }
    }

    if (!isTest && isJvm) {
        publishing.publish(
            this@subprojects,
            component = if (isPlatform) PublicationComponent.JavaPlatform else PublicationComponent.Java
        )
    }
}
