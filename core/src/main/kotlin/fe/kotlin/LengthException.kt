package fe.kotlin

@Throws(IllegalArgumentException::class)
internal fun throwListSizeError(
    needs: Int, has: Int
): Nothing = throw IllegalArgumentException("List must have at least $has elements, but only has $needs")
