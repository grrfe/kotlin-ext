package fe.kotlin.builder

import kotlin.experimental.ExperimentalTypeInference

@OptIn(ExperimentalTypeInference::class)
public inline fun <T> buildLazyList(
    mode: LazyThreadSafetyMode = LazyThreadSafetyMode.SYNCHRONIZED,
    @BuilderInference crossinline builderAction: MutableList<T>.() -> Unit,
): Lazy<List<T>> {
    return lazy(mode) { buildList(builderAction) }
}

@OptIn(ExperimentalTypeInference::class)
public inline fun <T> buildLazyList(
    mode: LazyThreadSafetyMode = LazyThreadSafetyMode.SYNCHRONIZED,
    capacity: Int,
    @BuilderInference crossinline builderAction: MutableList<T>.() -> Unit,
): Lazy<List<T>> {
    return lazy(mode) { buildList(capacity, builderAction) }
}
