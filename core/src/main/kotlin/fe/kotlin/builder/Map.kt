package fe.kotlin.builder

import kotlin.experimental.ExperimentalTypeInference

@OptIn(ExperimentalTypeInference::class)
public inline fun <K, V> buildLazyMap(
    mode: LazyThreadSafetyMode = LazyThreadSafetyMode.SYNCHRONIZED,
    @BuilderInference crossinline builderAction: MutableMap<K, V>.() -> Unit,
): Lazy<Map<K, V>> {
    return lazy(mode) { buildMap(builderAction) }
}

@OptIn(ExperimentalTypeInference::class)
public inline fun <K, V> buildLazyMap(
    mode: LazyThreadSafetyMode = LazyThreadSafetyMode.SYNCHRONIZED,
    capacity: Int,
    @BuilderInference crossinline builderAction: MutableMap<K, V>.() -> Unit,
): Lazy<Map<K, V>> {
    return lazy(mode) { buildMap(capacity, builderAction) }
}
