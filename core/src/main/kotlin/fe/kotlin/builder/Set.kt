package fe.kotlin.builder

import kotlin.experimental.ExperimentalTypeInference

@OptIn(ExperimentalTypeInference::class)
public inline fun <T> buildLazySet(
    mode: LazyThreadSafetyMode = LazyThreadSafetyMode.SYNCHRONIZED,
    @BuilderInference crossinline builderAction: MutableSet<T>.() -> Unit,
): Lazy<Set<T>> {
    return lazy(mode) { buildSet(builderAction) }
}

@OptIn(ExperimentalTypeInference::class)
public inline fun <T> buildLazySet(
    mode: LazyThreadSafetyMode = LazyThreadSafetyMode.SYNCHRONIZED,
    capacity: Int,
    @BuilderInference crossinline builderAction: MutableSet<T>.() -> Unit,
): Lazy<Set<T>> {
    return lazy(mode) { buildSet(capacity, builderAction) }
}
