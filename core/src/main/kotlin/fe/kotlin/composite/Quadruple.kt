package fe.kotlin.composite

import fe.kotlin.throwListSizeError
import java.io.Serializable

public data class Quadruple<out A, out B, out C, out D>(
    val first: A,
    val second: B,
    val third: C,
    val fourth: D
) : Serializable {
    override fun toString(): String = "($first, $second, $third, $fourth)"
}

public operator fun <A> Quadruple<A, *, *, *>?.component1(): A? = this?.component1()
public operator fun <B> Quadruple<*, B, *, *>?.component2(): B? = this?.component2()
public operator fun <C> Quadruple<*, *, C, *>?.component3(): C? = this?.component3()
public operator fun <D> Quadruple<*, *, *, D>?.component4(): D? = this?.component4()

public fun <T> List<T>.toQuadrupleOrNull(): Quadruple<T, T, T, T>? {
    return if (this.size >= 4) return Quadruple(this[0], this[1], this[2], this[3]) else null
}

@Throws(IllegalArgumentException::class)
public fun <T> List<T>.toQuadrupleOrThrow(): Quadruple<T, T, T, T> {
    return this.toQuadrupleOrNull() ?: throwListSizeError(size, 4)
}
