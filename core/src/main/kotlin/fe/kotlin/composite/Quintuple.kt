package fe.kotlin.composite

import fe.kotlin.throwListSizeError
import java.io.Serializable

public data class Quintuple<out A, out B, out C, out D, out E>(
    val first: A,
    val second: B,
    val third: C,
    val fourth: D,
    val fifth: E,
) : Serializable {
    override fun toString(): String = "($first, $second, $third, $fourth, $fifth)"

    public fun toList(): List<Any?> = listOf(first, second, third, fourth, fifth)
}

public operator fun <A> Quintuple<A, *, *, *, *>?.component1(): A? = this?.component1()
public operator fun <B> Quintuple<*, B, *, *, *>?.component2(): B? = this?.component2()
public operator fun <C> Quintuple<*, *, C, *, *>?.component3(): C? = this?.component3()
public operator fun <D> Quintuple<*, *, *, D, *>?.component4(): D? = this?.component4()
public operator fun <E> Quintuple<*, *, *, *, E>?.component5(): E? = this?.component5()


public fun <T> List<T>.toQuintupleOrNull(): Quintuple<T, T, T, T, T>? {
    return if (this.size >= 5) return Quintuple(this[0], this[1], this[2], this[3], this[4]) else null
}

@Throws(IllegalArgumentException::class)
public fun <T> List<T>.toQuintupleOrThrow(): Quintuple<T, T, T, T, T> {
    return this.toQuintupleOrNull() ?: throwListSizeError(size, 5)
}
