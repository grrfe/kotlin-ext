package fe.kotlin.extension

import java.io.File
import java.util.*

public fun Properties.writeTo(file: File, comments: String? = null) {
    file.outputStream().use { store(it, comments) }
}

public fun <K : Any, V : Any?> propertiesOfNotNull(vararg pairs: Pair<K, V>): Properties {
    return Properties().apply {
        putAll(mapOf(*pairs).filter { it.value != null })
    }
}
