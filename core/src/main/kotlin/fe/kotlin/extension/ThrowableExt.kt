package fe.kotlin.extension

import java.io.PrintWriter
import java.io.StringWriter
import java.io.Writer

public fun Throwable.asString(): String {
    return StringWriter().apply {
        writeTo(this)
    }.toString()
}

public fun Throwable.writeTo(writer: Writer) {
    PrintWriter(writer).use { printStackTrace(it) }
}
