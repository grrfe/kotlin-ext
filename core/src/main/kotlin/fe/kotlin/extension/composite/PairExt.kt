package fe.kotlin.extension.composite

import fe.kotlin.throwListSizeError


public fun <T> List<T>.toPairOrNull(): Pair<T, T>? {
    return if (this.size >= 2) return this[0] to this[1] else null
}

@Throws(IllegalArgumentException::class)
public fun <T> List<T>.toPairOrThrow(): Pair<T, T> {
    return this.toPairOrNull() ?: throwListSizeError(size, 2)
}

public operator fun <A> Pair<A, *>?.component1(): A? = null
public operator fun <B> Pair<*, B>?.component2(): B? = null

public inline fun <A, B, R> Pair<A?, B?>.allNotNull(block: (A, B) -> R): R? {
    return allNotNull(first, second, block)
}

public inline fun <A, B, R> allNotNull(first: A?, second: B?, block: (A, B) -> R): R? {
    return if (first != null && second != null) block(first, second) else null
}
