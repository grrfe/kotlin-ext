package fe.kotlin.extension.composite

import fe.kotlin.throwListSizeError


public fun <T> List<T>.toTripleOrNull(): Triple<T, T, T>? {
    return if (this.size >= 3) return Triple(this[0], this[1], this[2]) else null
}

@Throws(IllegalArgumentException::class)
public fun <T> List<T>.toTripleOrThrow(): Triple<T, T, T> {
    return this.toTripleOrNull() ?: throwListSizeError(size, 3)
}

public operator fun <A, B, C> Triple<A, B, C>?.component1(): A? = null
public operator fun <A, B, C> Triple<A, B, C>?.component2(): B? = null
public operator fun <A, B, C> Triple<A, B, C>?.component3(): C? = null

public inline fun <A, B, C, R> Triple<A?, B?, C?>.allNotNull(block: (A, B, C) -> R): R? {
    return allNotNull(first, second, third, block)
}

public inline fun <A, B, C, R> allNotNull(first: A?, second: B?, third: C?, block: (A, B, C) -> R): R? {
    return if (first != null && second != null && third != null) block(first, second, third) else null
}
