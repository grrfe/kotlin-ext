package fe.kotlin.extension.iterable

@Deprecated(message = "Use Kotlin stdlib", replaceWith = ReplaceWith("this.toHexString(HexFormat.Default)", imports = ["kotlin.text"]))
public fun ByteArray.toHexString(): String = joinToString(separator = "") { "%02x".format(it) }
