package fe.kotlin.extension.iterable

import fe.kotlin.stdlib.*
import fe.kotlin.`typealias`.KtPredicate


public inline fun <T, R, C> Iterable<T>.mapToOrDefault(
    destination: C,
    default: () -> C,
    transform: (T) -> R
): C where C : MutableCollection<R> {
    for (item in this) destination.add(transform(item))
    if (destination.isNotEmpty()) return default()

    return destination
}

//public inline fun <T, R, C : MutableCollection<R>> Iterable<T>.mapOrDefault(
//    default: () -> C,
//    transform: (T) -> R
//): C {
//    return mapToOrDefault(newMutableList(), default, transform) as C
//}

public inline fun <T, R> Iterable<T>.combineEach(
    other: Iterable<T>,
    transform: (item: T, otherItem: T) -> R,
): List<R> {
    val destination = newMutableList<R>()
    for (item in this) {
        for (otherItem in other) {
            destination.add(transform(item, otherItem))
        }
    }

    return destination
}

public inline fun <T, R, C : MutableCollection<R>> Iterable<T>.combineEachTo(
    destination: C,
    other: Iterable<T>,
    transform: (item: T, otherItem: T) -> R,
): C {
    for (item in this) {
        for (otherItem in other) {
            destination.add(transform(item, otherItem))
        }
    }

    return destination
}


public fun <T> Iterable<Result<T>>.map(): Pair<MutableList<T>, MutableList<Throwable>> {
    val successes = ArrayList<T>()
    val failures = ArrayList<Throwable>()

    for (result in this) {
        if (result.isSuccess) {
            successes.add(result.getOrNull()!!)
        } else if (result.isFailure) {
            failures.add(result.exceptionOrNull()!!)
        }
    }

    return successes to failures
}

public fun <T> Iterable<Result<T>>.toSuccess(): MutableList<T> {
    val destination = ArrayList<T>()
    for (result in this) {
        if (result.isSuccess) destination.add(result.getOrNull()!!)
    }

    return destination
}

public fun <T> Iterable<Result<T>>.toFailure(): MutableList<Throwable> {
    val destination = ArrayList<Throwable>()
    for (result in this) {
        if (result.isFailure) destination.add(result.exceptionOrNull()!!)
    }

    return destination
}

public inline fun <T> Iterable<Result<T>>.onEachSuccess(action: (value: T) -> Unit): Iterable<Result<T>> {
    for (result in this) result.onSuccess(action)
    return this
}

public inline fun <T> Iterable<Result<T>>.onEachFailure(action: (exception: Throwable) -> Unit): Iterable<Result<T>> {
    for (result in this) result.onFailure(action)
    return this
}

public inline fun <T, R : Any> Iterable<T>.mapCatching(transform: (T) -> R): List<Result<R>> {
    return mapCatchingTo(newMutableList(), transform)
}

public inline fun <T, R : Any, C : MutableCollection<Result<R>>> Iterable<T>.mapCatchingTo(
    destination: C,
    transform: (T) -> R,
): C {
    for (element in this) destination.add(runCatching { transform(element) })
    return destination
}

public inline fun <T, R : Any> Iterable<T>.mapValueNotNull(transform: (T) -> Pair<T, R?>): List<Pair<T, R>> {
    return mapValueNotNullTo(newMutableList(), transform)
}

public inline fun <T, R : Any, C : MutableCollection<Pair<T, R>>> Iterable<T>.mapValueNotNullTo(
    destination: C,
    transform: (T) -> Pair<T, R?>,
): C {
    for (element in this) {
        val pair = transform(element)
        @Suppress("UNCHECKED_CAST")
        if (pair.second != null) destination.add(pair as Pair<T, R>)
    }

    return destination
}

public inline fun <T> Iterable<T>.findWithIndexOrNull(predicate: KtPredicate<T>): Pair<T, Int>? {
    for ((index, element) in this.withIndex()) if (predicate(element)) return element to index
    return null
}

public inline fun <T, K, V, M> Iterable<T>.flatMap(
    destination: MutableMap<K, V> = newMutableMap(),
    transform: (T) -> Map<K, V>,
): M where M : Map<K, V> {
    for (item in this) destination.putAll(transform(item))

    @Suppress("UNCHECKED_CAST")
    return destination as M
}

public inline fun <T, R, S> Iterable<T>.mapToSet(
    destination: MutableSet<R> = newMutableSet(),
    transform: (T) -> R,
): S where S : Set<R> {
    @Suppress("UNCHECKED_CAST")
    return mapTo(destination, transform) as S
}

public inline fun <T> Iterable<T>.filterToSet(
    destination: MutableSet<T> = newMutableSet(),
    predicate: (T) -> Boolean,
): MutableSet<T> {
    return filterTo(destination, predicate)
}


public inline fun <T> Iterable<T>.filterIf(
    filter: Boolean,
    predicate: KtPredicate<T>,
): Iterable<T> = if (filter) filter(predicate) else this

public fun <T> Iterable<T>.filterIfPredicateIsNotNull(
    predicate: KtPredicate<T>? = null,
): Iterable<T> = if (predicate != null) filter(predicate) else this

/**
 * Works like the groupBy function from the Kotlin standard library, but null keys will be dropped with the caller
 * being notified when this occurs
 *
 * Also supports caching the key, so if it is an expensive operation, it will only get executed once.
 *
 * @receiver An Iterable<T> of type [T]
 * @param keySelector A function which transforms each item from [T] to the group key [K]
 * @param nullKeyHandler Handle [T]'s whose [keySelector] function yielded null
 * @param cacheIndexSelector A function which transforms each item from [T] to [C]; This function should not be expensive, like a just returning [T] itself or calling toString() on [T]
 * @param valueTransform A function which transforms each item from [T] to [V]
 * @param destination An instance of [MutableMap] where the results will be stored
 * @param collection A function providing a [MutableCollection] of type [V] on which will be used as the value specified in [destination]
 *
 * @return a [Map] with key type [K] and value type [Collection] of type [V]
 */
public inline fun <T, C, K, V, MM, MC> Iterable<T>.groupByNoNullKeys(
    keySelector: (T) -> K?,
    nullKeyHandler: ((T) -> Unit) = {},
    cacheIndexSelector: (T) -> C,
    valueTransform: (T) -> V,
    destination: MutableMap<K, MutableCollection<V>> = newMutableMap(),
    collection: () -> MutableCollection<V> = { linkedSetOf() },
): MM where MC : Collection<V>, MM : Map<K, MC> {
    val keySelectorCache = newMutableMap<C, K?>()
    for (element in this) {
        val cacheIndex = cacheIndexSelector(element)

        val cachedKey = keySelectorCache.getOrPut(cacheIndex) {
            val key = keySelector(element)
            if (key == null) {
                nullKeyHandler(element)
            }

            key
        }

        if (cachedKey != null) {
            destination.getOrPut(cachedKey, collection).add(valueTransform(element))
        }
    }

    @Suppress("UNCHECKED_CAST")
    return destination as MM
}

public inline fun <T, C, K, V, MM, MC> Iterable<T>.groupByNoNullKeys(
    keySelector: (T) -> K?,
    cacheIndexSelector: (T) -> C,
    valueTransform: (T) -> V,
    destination: MutableMap<K, MutableCollection<V>> = newMutableMap(),
    collection: () -> MutableCollection<V> = { linkedSetOf() },
): Pair<MM, List<T>> where MC : Collection<V>, MM : Map<K, MC> {
    val nulls = newMutableList<T>()
    val map = groupByNoNullKeys<T, C, K, V, MM, MC>(
        keySelector,
        nullKeyHandler = { nulls.add(it) },
        cacheIndexSelector,
        valueTransform,
        destination,
        collection
    )

    return map to nulls
}
