package fe.kotlin.extension.iterable

public fun <T : Any> List<T>.getOrFirstOrNull(index: Int): T? {
    return getOrNull(index) ?: firstOrNull()
}
