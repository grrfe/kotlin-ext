package fe.kotlin.extension.iterable

import java.util.*

public inline fun <T> Queue<T>.popAll(item: (T) -> Unit) {
    while (isNotEmpty()) {
        val head = poll()
        if (head != null) item(head)
    }
}
