package fe.kotlin.extension.iterable

public inline fun <T, R, S> Set<T>.map(
    destination: MutableSet<R> = linkedSetOf(),
    transform: (T) -> R
): S where S : Set<R> {
    for (item in this) destination.add(transform(item))

    @Suppress("UNCHECKED_CAST")
    return destination as S
}
