package fe.kotlin.extension.iterator

public data class ElementInfo<T>(val element: T, val index: Int, val isFirst: Boolean, val isLast: Boolean)

public inline fun <T> Array<T>.forEachWithInfo(action: (ElementInfo<T>) -> Unit) {
    withElementInfo().forEach(action)
}

public inline fun <T> Iterable<T>.forEachWithInfo(action: (ElementInfo<T>) -> Unit) {
    withElementInfo().forEach(action)
}

public fun <T> Array<T>.withElementInfo(): Iterable<ElementInfo<T>> {
    return ElementInfoIterable { iterator() }
}

public fun <T> Iterable<T>.withElementInfo(): Iterable<ElementInfo<T>> {
    return ElementInfoIterable { iterator() }
}

internal class ElementInfoIterable<T>(
    private val iteratorFactory: () -> Iterator<T>,
) : Iterable<ElementInfo<T>> {

    override fun iterator(): Iterator<ElementInfo<T>> {
        return ElementInfoIterator(iteratorFactory())
    }
}

internal class ElementInfoIterator<T>(
    private val iterator: Iterator<T>,
) : Iterator<ElementInfo<T>> {
    private var index = 0

    override fun hasNext(): Boolean {
        return iterator.hasNext()
    }

    override fun next(): ElementInfo<T> {
        val next = iterator.next()
        val isFirst = index == 0
        return ElementInfo(next, index++, isFirst, !hasNext())
    }
}




