package fe.kotlin.extension.map

import fe.kotlin.`typealias`.KtPredicate

public inline fun <K, V, M> Map<K, V>.filterIf(
    destination: MutableMap<K, V> = mutableMapOf(),
    condition: Boolean,
    predicate: KtPredicate<Map.Entry<K, V>>,
): M where M : Map<K, V> {
    val map = if (condition) filterTo(destination, predicate) else this

    @Suppress("UNCHECKED_CAST")
    return map as M
}

public fun <K, V, M> Map<K, V>.filterIfFilterIsNotNull(
    destination: MutableMap<K, V> = mutableMapOf(),
    predicate: KtPredicate<Map.Entry<K, V>>? = null
): M where M : Map<K, V> {
    val map = if (predicate != null) filterTo(destination, predicate) else this

    @Suppress("UNCHECKED_CAST")
    return map as M
}

public inline fun <K, V, R, M> Map<K, V?>.mapRemoveNullValues(
    destination: MutableMap<K, V?> = mutableMapOf(),
    transform: (V?) -> R
): M where M : Map<K, R> {
    for ((key, value) in this) {
        val transformed = transform(value)
        if (transformed != null) {
            destination[key] = value
        }
    }

    @Suppress("UNCHECKED_CAST")
    return destination as M
}

public inline fun <K, V> Map<K, V>.forEachValueNotNull(action: (Map.Entry<K, V>) -> Unit) {
    for (entry in this) {
        if (entry.value != null) action(entry)
    }
}
