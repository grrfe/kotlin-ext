package fe.kotlin.extension.map

public typealias MapValuePredicate<V> = (current: V, value: V) -> Boolean

public inline fun <K, V> MutableMap<K, V?>.putOrReplace(key: K, value: V, replaceIf: MapValuePredicate<V>): V {
    val currentValue = this[key]

    return if (currentValue == null || replaceIf(currentValue, value)) {
        put(key, value)
        value
    } else currentValue
}

public inline fun <K, V> MutableMap<K, V?>.putOrReplace(
    key: K,
    valueSelector: () -> V,
    replaceIf: MapValuePredicate<V>
): V {
    return putOrReplace(key, valueSelector(), replaceIf)
}

public inline fun <K, V> MutableMap<K, V?>.update(
    key: K,
    default: V,
    update: (V) -> V
): V {
    val value = this[key]
    val newValue = if (value == null) default else update(value)
    this[key] = newValue

    return newValue
}
