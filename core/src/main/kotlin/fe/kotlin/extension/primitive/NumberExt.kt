package fe.kotlin.extension.primitive

import fe.kotlin.`typealias`.KtPredicate

public inline fun <T : Number> T.takeIf(predicate: KtPredicate<T>): T? {
    return if (predicate(this)) this else null
}
