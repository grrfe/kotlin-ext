package fe.kotlin.extension.string

import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi

@ExperimentalEncodingApi
@Throws(IndexOutOfBoundsException::class, IllegalArgumentException::class)
public fun String.encodeBase64Throw(base64: Base64 = Base64.Default): String {
    return base64.encode(toByteArray())
}

@ExperimentalEncodingApi
@Throws(IndexOutOfBoundsException::class, IllegalArgumentException::class)
public fun String.decodeBase64Throw(base64: Base64 = Base64.Default): String {
    return String(base64.decode(toByteArray()))
}

@ExperimentalEncodingApi
public fun String.encodeBase64OrNull(base64: Base64 = Base64.Default): String? {
    return runCatching { encodeBase64Throw(base64) }.getOrNull()
}

@ExperimentalEncodingApi
public fun String.decodeBase64OrNull(base64: Base64 = Base64.Default): String? {
    return runCatching { decodeBase64Throw(base64) }.getOrNull()
}
