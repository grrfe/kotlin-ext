package fe.kotlin.extension.string

@Deprecated("Use experimental Kotlin Hex API instead", ReplaceWith("hexToByteArray()"))
@Throws(IllegalArgumentException::class)
public fun String.decodeHexOrThrow(): ByteArray {
    check(length % 2 == 0) { "String length ($length) must be even" }

    return ByteArray(length / 2) { index ->
        substring(index * 2, (index + 1) * 2).toInt(16).toByte()
    }
}

public fun String.decodeHexOrNull(): ByteArray? {
    return runCatching { decodeHexOrThrow() }.getOrNull()
}
