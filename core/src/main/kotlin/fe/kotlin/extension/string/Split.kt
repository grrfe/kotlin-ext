package fe.kotlin.extension.string

import fe.kotlin.extension.primitive.takeIf

public fun String.splitAtIndexOfOrNull(
    str: String,
    startIndex: Int = lastIndex,
    ignoreCase: Boolean = false,
): Pair<String, String>? {
    val index = indexOf(str, startIndex, ignoreCase).takeIf { it > -1 } ?: return null
    return splitAtOrThrow(index)
}

public fun String.splitAtLastIndexOfOrNull(
    str: String,
    startIndex: Int = lastIndex,
    ignoreCase: Boolean = false,
): Pair<String, String>? {
    val index = lastIndexOf(str, startIndex, ignoreCase).takeIf { it > -1 } ?: return null
    return splitAtOrThrow(index)
}

public fun String.splitAtOrThrow(index: Int): Pair<String, String> {
    return substring(0, index) to substring(index + 1)
}
