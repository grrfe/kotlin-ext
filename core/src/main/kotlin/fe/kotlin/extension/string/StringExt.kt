package fe.kotlin.extension.string

import java.util.*

public fun String.capitalize(locale: Locale = Locale.getDefault()): String {
    return replaceFirstChar { if (it.isLowerCase()) it.titlecase(locale) else it.toString() }
}

public fun String.prependIfNotStartsWith(start: String): String {
    return if (!this.startsWith(start)) start + this else this
}

public fun String.appendIfNotEndsWith(end: String): String {
    return if (!this.endsWith(end)) this + end else this
}

