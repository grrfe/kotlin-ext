package fe.kotlin.extension.string

public fun String.substringOrNull(startIndex: Int, endIndex: Int): String? {
    return runCatching { substring(startIndex, endIndex) }.getOrNull()
}

public fun String.substringOrNull(startIndex: Int): String? {
    return runCatching { substring(startIndex) }.getOrNull()
}
