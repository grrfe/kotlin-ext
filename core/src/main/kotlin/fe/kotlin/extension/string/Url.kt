package fe.kotlin.extension.string

import java.net.URLDecoder
import java.net.URLEncoder
import java.nio.charset.Charset

public fun String.encodeUrl(charset: Charset = Charsets.UTF_8): String {
    return URLEncoder.encode(this, charset)
}

public fun String.decodeUrl(charset: Charset = Charsets.UTF_8): String {
    return URLDecoder.decode(this, charset)
}
