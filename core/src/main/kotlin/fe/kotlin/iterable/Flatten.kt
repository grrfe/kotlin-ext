package fe.kotlin.iterable

public fun <T> flattened(
    vararg elements: Iterable<T>,
    destination: MutableCollection<T> = mutableSetOf(),
): MutableCollection<T> {
    for (element in elements) destination.addAll(element)
    return destination
}
