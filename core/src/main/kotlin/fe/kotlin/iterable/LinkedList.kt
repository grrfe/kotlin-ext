package fe.kotlin.iterable

import java.util.*

public fun <T> linkedList(vararg elements: T): LinkedList<T> {
    val list = LinkedList<T>()
    for (element in elements) list.add(element)
    return list
}
