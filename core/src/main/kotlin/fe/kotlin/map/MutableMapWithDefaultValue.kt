package fe.kotlin.map

import java.util.function.BiFunction
import java.util.function.Function

@Target(
    AnnotationTarget.CLASS,
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY,
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.CONSTRUCTOR,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.TYPEALIAS
)
@RequiresOptIn
@MustBeDocumented
public annotation class DiscouragedDefaultValueMapMethod(
    val message: String,
    val replaceWith: ReplaceWith = ReplaceWith(""),
    val level: DeprecationLevel = DeprecationLevel.WARNING
)

public class MutableMapWithDefaultValue<K, V>(
    public val initializer: (K) -> V,
    private val map: MutableMap<K, V> = mutableMapOf(),
    private val putValueStrategy: PutValueStrategy = PutValueStrategy.NoOp,
) : MutableMap<K, V> by map {
    public enum class PutValueStrategy {
        NoOp, Throw, Replace
    }

    public companion object {
        public const val MESSAGE: String = "Method either is a no-op, replaces existing elements or throws an exception, depending on how the Map was configured. Should not be used at all."
        public const val EXCEPTION_MESSAGE: String = "Values in this Map are auto initialized. You may not set them yourself."
    }

    @Suppress("MemberVisibilityCanBePrivate")
    public fun getOrInit(key: K): V {
        var value = map[key]
        if (value == null) {
            value = initializer(key)
            map[key] = value
        }

        return value!!
    }

    @DiscouragedDefaultValueMapMethod(
        message = "While this method works fine, especially if it is used as an array accessor, it might confuse people",
        replaceWith = ReplaceWith("getOrInit(key)"),
        level = DeprecationLevel.WARNING
    )
    override operator fun get(key: K): V {
        return getOrInit(key)
    }

    @DiscouragedDefaultValueMapMethod(
        message = MESSAGE,
        level = DeprecationLevel.WARNING
    )
    public operator fun set(key: K, value: V): V? {
        if (putValueStrategy == PutValueStrategy.Throw) throw IllegalArgumentException(EXCEPTION_MESSAGE)
        else if (putValueStrategy == PutValueStrategy.Replace) {
            map[key] = value
            return value
        }

        return null
    }

    @DiscouragedDefaultValueMapMethod(message = MESSAGE, level = DeprecationLevel.WARNING)
    override fun put(key: K, value: V): V? {
        return set(key, value)
    }

    @DiscouragedDefaultValueMapMethod(message = MESSAGE, level = DeprecationLevel.WARNING)
    override fun putAll(from: Map<out K, V>) {
        from.forEach { (key, value) -> set(key, value) }
    }

    @DiscouragedDefaultValueMapMethod(message = MESSAGE, level = DeprecationLevel.WARNING)
    override fun putIfAbsent(key: K, value: V): V? {
        var mapValue = map[key]
        if (mapValue == null) {
            mapValue = set(key, value)
        }

        return mapValue
    }

    @DiscouragedDefaultValueMapMethod(message = MESSAGE, level = DeprecationLevel.WARNING)
    override fun compute(key: K, remappingFunction: BiFunction<in K, in V?, out V?>): V? {
        return throwOrReplace(null) { super.compute(key, remappingFunction) } ?: getOrInit(key)

    }

    @DiscouragedDefaultValueMapMethod(message = MESSAGE, level = DeprecationLevel.WARNING)
    override fun computeIfAbsent(key: K, mappingFunction: Function<in K, out V>): V {
        return throwOrReplace(null) { super.computeIfAbsent(key, mappingFunction) } ?: getOrInit(key)

    }

    @DiscouragedDefaultValueMapMethod(message = MESSAGE, level = DeprecationLevel.WARNING)
    override fun computeIfPresent(key: K, remappingFunction: BiFunction<in K, in V & Any, out V?>): V? {
        return throwOrReplace(null) { super.computeIfPresent(key, remappingFunction) } ?: getOrInit(key)
    }

    @DiscouragedDefaultValueMapMethod(message = MESSAGE, level = DeprecationLevel.WARNING)
    override fun merge(key: K, value: V & Any, remappingFunction: BiFunction<in V & Any, in V & Any, out V?>): V? {
        return throwOrReplace(null) { super.merge(key, value, remappingFunction) } ?: getOrInit(key)
    }

    @DiscouragedDefaultValueMapMethod(message = MESSAGE, level = DeprecationLevel.WARNING)
    override fun replace(key: K, value: V): V {
        return throwOrReplace(null) { super.replace(key, value) } ?: getOrInit(key)
    }

    @DiscouragedDefaultValueMapMethod(message = MESSAGE, level = DeprecationLevel.WARNING)
    override fun replace(key: K, oldValue: V, newValue: V): Boolean {
        return throwOrReplace(true) { super.replace(key, oldValue, newValue) }!!
    }

    @DiscouragedDefaultValueMapMethod(message = MESSAGE, level = DeprecationLevel.WARNING)
    override fun replaceAll(function: BiFunction<in K, in V, out V>) {
        throwOrReplace { super.replaceAll(function) }
    }

    private inline fun <T : Any> throwOrReplace(defaultReturn: T? = null, replace: () -> T?): T? {
        if (putValueStrategy == PutValueStrategy.Throw) throw IllegalArgumentException(EXCEPTION_MESSAGE)
        if (putValueStrategy == PutValueStrategy.Replace) {
            return replace()
        }

        return defaultReturn
    }
}

public fun <K, V> mutableMapWithDefaultValue(initializer: (K) -> V): MutableMapWithDefaultValue<K, V> {
    return MutableMapWithDefaultValue(initializer)
}

public fun <K, C : MutableCollection<*>> mutableMapWithMutableCollectionValue(initializer: (K) -> C): MutableMapWithDefaultValue<K, C> {
    return MutableMapWithDefaultValue(initializer)
}

public fun <K, V> mutableMapWithMutableListValue(): MutableMapWithDefaultValue<K, MutableList<V>> {
    return MutableMapWithDefaultValue(initializer = { mutableListOf() })
}
