package fe.kotlin.stdlib

public fun <T> Iterable<*>.newMutableList(): MutableList<T> {
    return ArrayList(collectionSizeOrDefault(10))
}

public fun <K, V> Iterable<*>.newMutableMap(): MutableMap<K, V> {
    return LinkedHashMap(mapCapacity(collectionSizeOrDefault(10)).coerceAtLeast(16))
}

public fun <T> Iterable<*>.newMutableSet(): MutableSet<T> {
    return HashSet(mapCapacity(collectionSizeOrDefault(12)))
}
