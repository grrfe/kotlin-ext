package fe.kotlin.stdlib


// Stuff "stolen" from Kotlin's stdlib which are not accessible from normal applications
public fun <T> Iterable<T>.collectionSizeOrDefault(default: Int = 16): Int = if (this is Collection<T>) this.size else default

// Maybe this should be kept up to date with
// https://github.com/JetBrains/kotlin/blob/5e63f7627fea8d044011f6b885406209cc471964/libraries/stdlib/jvm/src/kotlin/collections/MapsJVM.kt#L147C21-L147C21
public fun mapCapacity(expectedSize: Int): Int = when {
    // We are not coercing the value to a valid one and not throwing an exception. It is up to the caller to
    // properly handle negative values.
    expectedSize < 0 -> expectedSize
    expectedSize < 3 -> expectedSize + 1
    expectedSize < INT_MAX_POWER_OF_TWO -> ((expectedSize / 0.75F) + 1.0F).toInt()
    // any large value
    else -> Int.MAX_VALUE
}

private const val INT_MAX_POWER_OF_TWO: Int = 1 shl (Int.SIZE_BITS - 2)
