package fe.kotlin.`typealias`

public typealias KtPredicate<T> = (T) -> Boolean
