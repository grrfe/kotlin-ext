package fe.kotlin.util

public inline fun <R> runIfOrNull(condition: Boolean, block: () -> R): R? = if (condition) block() else null

public inline fun runIf(condition: Boolean, block: () -> Unit): Unit = if (condition) block() else Unit

public inline fun <T> T.applyIf(condition: Boolean, block: T.() -> Unit): T {
    if (condition) block()
    return this
}

public fun <T> T.applyIfNotNull(block: (T.() -> Unit)? = null): T {
    if (block != null) block()
    return this
}
