package fe.std.iterable

public fun <K, V> Iterable<Map<K, V>>.merge(
    destination: MutableMap<K, V> = LinkedHashMap(),
): MutableMap<K, V> {
    for (element in this) {
        for ((key, value) in element) {
            destination[key] = value
        }
    }

    return destination
}
