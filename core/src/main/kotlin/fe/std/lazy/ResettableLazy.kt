package fe.std.lazy

// Inspired by https://github.com/signalapp/Signal-Android/blob/main/core-util-jvm/src/main/java/org/signal/core/util/ResettableLazy.kt
public fun <T> resettableLazy(initializer: () -> T): ResettableLazy<T> = ResettableSynchronizedLazy(initializer)

public interface ResettableLazy<out T> : Lazy<T> {
    public fun reset()
    public fun refresh(): T
}

internal object UNINITIALIZED_VALUE

public class ResettableSynchronizedLazy<out T>(initializer: () -> T, lock: Any? = null) : ResettableLazy<T> {
    private var initializer: (() -> T)? = initializer

    @Volatile
    private var _value: Any? = UNINITIALIZED_VALUE

    // final field to ensure safe publication of 'SynchronizedLazyImpl' itself through
    // var lazy = lazy() {}
    private val lock = lock ?: this

    override val value: T
        get() {
            val _v1 = _value
            if (_v1 !== UNINITIALIZED_VALUE) {
                @Suppress("UNCHECKED_CAST")
                return _v1 as T
            }

            return synchronized(lock) {
                val _v2 = _value
                if (_v2 !== UNINITIALIZED_VALUE) {
                    @Suppress("UNCHECKED_CAST") (_v2 as T)
                } else {
                    val typedValue = initializer!!()
                    _value = typedValue
                    typedValue
                }
            }
        }

    public override fun reset() {
        if (_value === UNINITIALIZED_VALUE) return

        synchronized(lock) {
            _value = UNINITIALIZED_VALUE
        }
    }

    override fun refresh(): T {
        return synchronized(lock) {
            val typedValue = initializer!!()
            _value = typedValue
            typedValue
        }
    }

    override fun isInitialized(): Boolean = _value !== UNINITIALIZED_VALUE

    override fun toString(): String = if (isInitialized()) value.toString() else "Lazy value not initialized yet."
}
