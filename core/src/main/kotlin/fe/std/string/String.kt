package fe.std.string

public fun <T> joinToString(
    vararg items: T,
    separator: CharSequence = ", ",
    prefix: CharSequence = "",
    postfix: CharSequence = "",
    limit: Int = -1,
    truncated: CharSequence = "...",
    transform: ((T) -> CharSequence)? = null,
): String {
    return items.joinToString(separator, prefix, postfix, limit, truncated, transform = transform)
}
