package fe.kotlin.extension.iterable

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNull
import kotlin.test.Test

internal class ListExtKtTest {
    @Test
    fun getOrFirstOrNull() {
        assertThat(emptyList<String>().getOrFirstOrNull(0)).isNull()
        assertThat(listOf("item 1").getOrFirstOrNull(0)).isEqualTo("item 1")
        assertThat(listOf("item 1").getOrFirstOrNull(1)).isEqualTo("item 1")
        assertThat(listOf("item 1", "item 2").getOrFirstOrNull(1)).isEqualTo("item 2")
    }
}
