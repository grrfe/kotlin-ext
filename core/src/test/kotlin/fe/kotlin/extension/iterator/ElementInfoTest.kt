package fe.kotlin.extension.iterator

import assertk.assertThat
import assertk.assertions.containsOnly
import kotlin.test.Test


internal class ElementInfoTest {
    @Test
    fun test() {
        val list = listOf("element 1", "element 2", "element 3")

        assertThat(list.withElementInfo()).containsOnly(
            ElementInfo("element 1", 0, true, false),
            ElementInfo("element 2", 1, false, false),
            ElementInfo("element 3", 2, false, true),
        )
    }
}
