package fe.std.iterable

import assertk.assertThat
import assertk.assertions.containsOnly
import org.junit.Test

class IterableTest {
    @Test
    fun `merge - non-duplicate key`() {
        val it = listOf(mapOf("hello" to "world"), mapOf("foo" to "bar"))
        val result = it.merge()
        assertThat(result).containsOnly("hello" to "world", "foo" to "bar")
    }

    @Test
    fun `merge - duplicate key`() {
        val it = listOf(mapOf("hello" to "world"), mapOf("hello" to "bar"))
        val result = it.merge()
        assertThat(result).containsOnly("hello" to "bar")
    }
}
