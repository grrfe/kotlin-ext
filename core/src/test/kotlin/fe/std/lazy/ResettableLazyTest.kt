package fe.std.lazy

import assertk.assertThat
import assertk.assertions.isEqualTo
import kotlin.test.Test

class ResettableLazyTest {
    private class ReadTracker<T>(initial: T) {
        var readCount = 0
            private set

        var current: T = initial
            get() {
                readCount++
                return field
            }
    }

    @Test
    fun `assert value never read more than once`() {
        var latch = ReadTracker(0)
        val lazy = resettableLazy { latch.current }

        assertThat(lazy.value).isEqualTo(0)
        assertThat(latch.readCount).isEqualTo(1)

        for (i in 0..3) {
            lazy.value
        }

        assertThat(latch.readCount).isEqualTo(1)
    }

    @Test
    fun `assert reset re-reads value`() {
        var latch = ReadTracker(0)
        val lazy = resettableLazy { latch.current }

        assertThat(lazy.value).isEqualTo(0)
        assertThat(latch.readCount).isEqualTo(1)

        latch.current = 1
        lazy.reset()

        assertThat(lazy.value).isEqualTo(1)
        assertThat(latch.readCount).isEqualTo(2)
    }
}
