package fe.std.dom.extension

import org.w3c.dom.Attr
import org.w3c.dom.NamedNodeMap
import org.w3c.dom.Node

public fun NamedNodeMap.asIterable(): Iterable<Node> {
    return Iterable { iterator() }
}

public operator fun NamedNodeMap.iterator(): Iterator<Node> {
    return object : Iterator<Node> {
        var i = 0

        override fun hasNext(): Boolean {
            return i < length - 1
        }

        override fun next(): Node {
            return item(i++)
        }
    }
}

public fun NamedNodeMap.toAttrMap(): Map<String, String> = buildMap {
    for (node in this@toAttrMap.asIterable()) {
        if (node !is Attr) continue
        this[node.name] = node.value
    }
}
