package fe.std.dom.extension

import org.w3c.dom.Node

public fun Node.attr(name: String): String? {
    return runCatching { attributes.getNamedItem(name).nodeValue }.getOrNull()
}
