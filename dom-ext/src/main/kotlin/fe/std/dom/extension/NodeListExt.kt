package fe.std.dom.extension

import org.w3c.dom.Node
import org.w3c.dom.NodeList

public fun NodeList.asIterable(): Iterable<Node> {
    return Iterable { iterator() }
}

public operator fun NodeList.iterator(): Iterator<Node> {
    return object : Iterator<Node> {
        var i = 0

        override fun hasNext(): Boolean {
            return i < length - 1
        }

        override fun next(): Node {
            return item(i++)
        }
    }
}
