package fe.std.io

import java.io.File
import java.util.*

@Deprecated(
    message = "Use new API",
    replaceWith = ReplaceWith("walk(WalkDepth.Direct, FileWalkDirection.TOP_DOWN)", imports = ["kotlin.io", "fe.std.io.WalkDepth"])
)
public fun File.walkTopDown(recursive: Boolean): Sequence<File> {
    return walk(if (recursive) WalkDepth.Recursive else WalkDepth.Direct, FileWalkDirection.TOP_DOWN)
}

@Deprecated(
    message = "Use new API",
    replaceWith = ReplaceWith("walk(WalkDepth.Direct, FileWalkDirection.TOP_DOWN)", imports = ["kotlin.io", "fe.std.io.WalkDepth"])
)
public fun File.children(recursive: Boolean = true): List<File> {
    return walkTopDown().maxDepth(if (recursive) Int.MAX_VALUE else 1).filter { it != this }.toList()
}

@JvmInline
public value class WalkDepth(public val value: Int) {
    public companion object {
        public val Direct: WalkDepth = WalkDepth(1)
        public val Recursive: WalkDepth = WalkDepth(Int.MAX_VALUE)
    }
}

public fun File.walk(depth: WalkDepth = WalkDepth.Recursive, direction: FileWalkDirection = FileWalkDirection.TOP_DOWN): Sequence<File> {
    return walk(direction)
        .maxDepth(depth.value)
        .filter { it != this }
}

public fun File.isChildOf(parent: File, recursive: Boolean = true): Boolean {
    if (!parent.isDirectory || this == parent) return false

    var childParent = parentFile
    while (childParent != null) {
        if (childParent == parent) return true
        if (!recursive) return false

        childParent = childParent.parentFile
    }

    return false
}

public fun File.mkdirsOrClean(): Boolean {
    return if (exists()) deleteChildren()
    else mkdirs()
}

public fun File.deleteChildren(): Boolean {
    return walk(WalkDepth.Direct, FileWalkDirection.BOTTOM_UP).fold(true) { result, file ->
        file.deleteRecursively() && result
    }
}

public fun File.readPropertiesOrNull(): Properties? {
    return if (exists()) inputStream().use { Properties().apply { load(it) } } else null
}
