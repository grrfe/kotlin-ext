package fe.std.io

import java.io.IOException
import java.nio.file.DirectoryStream
import java.nio.file.Path
import kotlin.io.path.*

public typealias PathFilter = (Path) -> Boolean

@Throws(IOException::class)
public inline fun Path.forEachDirectoryEntry(noinline filter: PathFilter, action: (Path) -> Unit) {
    return newDirectoryStream(filter).use { it.forEach(action) }
}

@Throws(IOException::class)
public fun Path.newDirectoryStream(filter: PathFilter): DirectoryStream<Path> {
    return fileSystem.provider().newDirectoryStream(this, filter)
}

public fun Path.recursiveFileSize(): Long {
    if (!isDirectory()) {
        return if (notExists()) 0L else fileSize()
    }

    return useDirectoryEntries { files ->
        files.sumOf { it.recursiveFileSize() }
    }
}
