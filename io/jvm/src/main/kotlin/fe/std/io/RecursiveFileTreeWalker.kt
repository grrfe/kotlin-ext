package fe.std.io

import java.io.IOException
import java.nio.file.Path
import kotlin.io.path.isDirectory

public typealias ExceptionHandler = (Path, Exception) -> Unit

public val PrintStackTraceExceptionHandler: ExceptionHandler = { _, e -> e.printStackTrace() }

public class RecursiveFileTreeWalker(
    private val path: Path,
    private val walkDirection: FileWalkDirection,
    private val filter: PathFilter,
    private val onException: ExceptionHandler? = null,
) : Sequence<Path> {

    public companion object {
        private val anyDirectoryFilter: PathFilter = { it.isDirectory() }
    }

    override fun iterator(): Iterator<Path> {
        return iterator { walk(path) }
    }

    private suspend fun SequenceScope<Path>.walk(path: Path) {
        try {
            if (walkDirection == FileWalkDirection.TOP_DOWN) {
                walkRecursive(path)
            }

            path.forEachDirectoryEntry(filter = filter) { yield(it) }

            if (walkDirection == FileWalkDirection.BOTTOM_UP) {
                walkRecursive(path)
            }
        } catch (e: IOException) {
            onException?.invoke(path, e)
        }
    }

    private suspend fun SequenceScope<Path>.walkRecursive(dirPath: Path) {
        try {
            dirPath.forEachDirectoryEntry(filter = anyDirectoryFilter) { walk(it) }
        } catch (e: IOException) {
            onException?.invoke(dirPath, e)
        }
    }
}

public fun Path.walkRecursive(
    walkDirection: FileWalkDirection = FileWalkDirection.TOP_DOWN,
    onException: ExceptionHandler? = PrintStackTraceExceptionHandler,
    filter: (Path) -> Boolean,
): Sequence<Path> {
    return RecursiveFileTreeWalker(
        this,
        walkDirection,
        filter,
        onException = onException
    )
}
