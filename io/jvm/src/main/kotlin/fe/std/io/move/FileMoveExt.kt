package fe.std.io.move

import java.io.File
import java.nio.file.Path

public fun File.moveInto(dir: File, overwrite: Boolean = false): FileMove<Path> {
    return toPath().moveInto(dir.toPath(), overwrite = overwrite)
}

public fun File.copyInto(dir: File, overwrite: Boolean = false): FileMove<Path> {
    return toPath().copyInto(dir.toPath(), overwrite = overwrite)
}
