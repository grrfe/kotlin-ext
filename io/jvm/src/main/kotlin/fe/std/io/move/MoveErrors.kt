package fe.std.io.move

import java.io.IOException
import java.lang.UnsupportedOperationException
import java.nio.file.AtomicMoveNotSupportedException
import java.nio.file.DirectoryNotEmptyException

public sealed interface FileMove<out T>

public data class Result<out T>(val result: T) : FileMove<T>

public data class AtomicMoveNotSupported<T>(
    val file: String?,
    val other: String? = null,
    val reason: String? = null,
) : FileMove<T>

public data class FileAlreadyExists<T>(
    val file: String?,
    val other: String? = null,
    val reason: String? = null,
) : FileMove<T>

public data class DirectoryNotEmpty<T>(
    val file: String?,
    val other: String? = null,
    val reason: String? = null,
) : FileMove<T>

public data class NoSuchFile<T>(
    val file: String?,
    val other: String? = null,
    val reason: String? = null,
) : FileMove<T>

public data class AccessDenied<T>(
    val file: String?,
    val other: String? = null,
    val reason: String? = null,
) : FileMove<T>

public data class UnsupportedOperation<T>(
    val message: String?,
    val cause: Throwable?,
) : FileMove<T>

public data class SecurityError<T>(
    val message: String?,
    val cause: Throwable?,
) : FileMove<T>

public data class OtherIOError<T>(
    val ex: IOException?,
) : FileMove<T>

public data class UnknownError<T>(
    val ex: Exception,
) : FileMove<T>

public fun <T> handleFileException(ex: Exception): FileMove<T> {
    return when (ex) {
        is AtomicMoveNotSupportedException -> AtomicMoveNotSupported(ex.file, ex.otherFile, ex.reason)
        is FileAlreadyExistsException -> FileAlreadyExists(ex.file.path, ex.other?.path, ex.reason)
        is java.nio.file.FileAlreadyExistsException -> FileAlreadyExists(ex.file, ex.otherFile, ex.reason)
        is DirectoryNotEmptyException -> DirectoryNotEmpty(ex.file, ex.otherFile, ex.reason)
        is NoSuchFileException -> NoSuchFile(ex.file.path, ex.other?.path, ex.reason)
        is java.nio.file.NoSuchFileException -> NoSuchFile(ex.file, ex.otherFile, ex.reason)
        is AccessDeniedException -> AccessDenied(ex.file.path, ex.other?.path, ex.reason)
        is UnsupportedOperationException -> UnsupportedOperation(ex.message, ex.cause)
        is SecurityException -> SecurityError(ex.message, ex.cause)
        is IOException -> OtherIOError(ex)
        else -> UnknownError(ex)
    }
}
