package fe.std.io.move

import java.nio.file.Path
import kotlin.io.path.copyTo
import kotlin.io.path.div
import kotlin.io.path.moveTo
import kotlin.io.path.name

public fun Path.moveInto(dir: Path, overwrite: Boolean = false): FileMove<Path> {
    try {
        val result = moveTo(dir / name, overwrite = overwrite)
        return Result(result)
    } catch (ex: Exception) {
        return handleFileException(ex)
    }
}

public fun Path.copyInto(dir: Path, overwrite: Boolean = false): FileMove<Path> {
    try {
        val result = copyTo(dir / name, overwrite = overwrite)
        return Result(result)
    } catch (ex: Exception) {
        return handleFileException(ex)
    }
}
