package fe.std.io

import java.io.File
import kotlin.test.Test
import kotlin.test.assertEquals

class FileExtTest {

    data class ChildOfInput(val file: File, val parent: File)
    data class ChildOfExpected(val recursiveTrue: Boolean, val recursiveFalse: Boolean) {
        companion object {
            val BothFalse = ChildOfExpected(recursiveTrue = false, recursiveFalse = false)
            val BothTrue = ChildOfExpected(recursiveTrue = true, recursiveFalse = true)
        }
    }

    @Test
    fun testIsChildOf() {
        val grrfe = File("/tmp/grrfe/")
        val kotlinExt = File(grrfe, "kotlin-ext").also { it.mkdirs() }
        val file = File(kotlinExt, "file1")

        mapOf(
            ChildOfInput(file, file) to ChildOfExpected.BothFalse,
            ChildOfInput(kotlinExt, kotlinExt) to ChildOfExpected.BothFalse,
            ChildOfInput(file, kotlinExt) to ChildOfExpected.BothTrue,
            ChildOfInput(file, grrfe) to ChildOfExpected(recursiveTrue = true, recursiveFalse = false)
        ).forEach { (input, expected) ->
            assertEquals(expected.recursiveTrue, input.file.isChildOf(input.parent, true))
            assertEquals(expected.recursiveFalse, input.file.isChildOf(input.parent, false))
        }

        grrfe.deleteRecursively()
    }
}
