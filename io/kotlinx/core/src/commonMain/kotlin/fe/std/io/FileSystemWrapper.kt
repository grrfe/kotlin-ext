package fe.std.io

import kotlinx.io.RawSink
import kotlinx.io.RawSource
import kotlinx.io.files.FileSystem
import kotlinx.io.files.Path
import kotlinx.io.files.SystemFileSystem

// FileSystem is sealed (https://github.com/Kotlin/kotlinx-io/issues/341), use wrapper until API is stable
interface FileSystemWrapper {
    fun sink(path: Path, append: Boolean): RawSink
    fun source(path: Path): RawSource
    fun resolve(path: Path): Path
    fun list(directory: Path): Collection<Path>
}

class DelegatingFileSystemWrapper(
    val fs: FileSystem = SystemFileSystem,
) : FileSystemWrapper {
    override fun sink(path: Path, append: Boolean): RawSink = fs.sink(path, append)

    override fun source(path: Path): RawSource = fs.source(path)

    override fun resolve(path: Path): Path = fs.resolve(path)

    override fun list(directory: Path): Collection<Path> = fs.list(directory)
}
