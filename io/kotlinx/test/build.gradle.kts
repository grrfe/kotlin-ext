plugins {
    kotlin("multiplatform")
}

kotlin {
    js()
    jvm()

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-io-core:0.6.0")
            }
        }

        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation("com.willowtreeapps.assertk:assertk:0.28.1")
            }
        }

        val jsMain by getting {
            dependencies {

            }
        }

        val jvmMain by getting {
            dependencies {
            }
        }
    }
}

//dependencies {
//    testImplementation(kotlin("test"))
//}
//
