package fe.std.io

import kotlinx.io.Buffer
import kotlinx.io.RawSource
import kotlinx.io.writeString

class MockRawSource(val value: String) : RawSource {
    private val buffer = Buffer().apply {
        writeString(value)
    }

    override fun readAtMostTo(sink: Buffer, byteCount: Long): Long {
        return buffer.readAtMostTo(sink, byteCount)
    }

    override fun close() {}
}
