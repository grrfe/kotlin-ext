package fe.std.process

import fe.std.process.sink.Disposable
import fe.std.process.sink.LineSink
import fe.std.process.sink.ProcessSink
import fe.std.process.sink.StatusChanged
import java.lang.ProcessBuilder
import java.util.*

private inline fun <reified T : ProcessSink> MutableList<ProcessSink>.send(action: (T) -> Unit) {
    getAll<T>().forEach(action)
}

private inline fun <reified T : ProcessSink> MutableList<ProcessSink>.getAll(): List<T> {
    return mapNotNull { it as? T }
}

public class ProcessHandler(
    private val processBuilder: ProcessBuilder,
    private val sinks: MutableList<ProcessSink> = mutableListOf()
) : Runnable {

    public constructor(args: List<String>, hooks: List<ProcessSink>) : this(
        ProcessBuilder(args).redirectErrorStream(true),
        hooks.toMutableList()
    )

    public constructor(args: List<String>) : this(ProcessBuilder(args).redirectErrorStream(true))

    private lateinit var process: Process
    public var status: ProcessStatus = Config
        private set(new) {
            val old = field
            field = new
            sinks.send<StatusChanged> { it.changed(this, old, new) }

            if (status is Stopped) {
                sinks.getAll<Disposable>().forEach { it.dispose() }
                sinks.clear()
            }
        }

    public fun withSink(sink: ProcessSink): ProcessHandler {
        if (status != Config) return this

        sinks.add(sink)
        return this
    }

    public fun start(config: ProcessStartConfig<*>): Int? {
        if (status != Config) return null

        process = processBuilder.start()
        status = config.start(process, this)

        val exitCode = process.waitFor()
        status = Stopped(exitCode)

        return exitCode
    }

    public fun stop() {
        if (status is Started) {
            process.destroy()
        }
    }

    override fun run() {
        val readers = sinks.getAll<LineSink>().toMutableList()
        val scanner = Scanner(process.inputStream)
        while (scanner.hasNextLine()) {
            val line = scanner.nextLine()
            readers.removeIf { it.receive(this, line) }
        }

        scanner.close()
    }
}
