package fe.std.process

import fe.std.process.sink.AllLinesSink
import java.io.PrintStream

public fun launchProcess(vararg args: String, config: ProcessStartConfig<*>, sink: AllLinesSink): Int {
    val handler = ProcessHandler(args.toList()).withSink(sink)
    return handler.start(config)!!
}

public fun launchProcess(
    vararg args: String,
    config: ProcessStartConfig<*>,
    printTo: PrintStream = System.out,
    singleLine: Boolean = false,
): Int {
    val sink = object : AllLinesSink {
        override fun handle(handler: ProcessHandler, line: String) {
            if (singleLine) printTo.print("$line\r")
            else printTo.println(line)
        }
    }
    val exitCode = launchProcess(*args, config = config, sink = sink)
    if (singleLine) println()
    return exitCode
}

public fun launchProcess(
    vararg args: String,
    config: ProcessStartConfig<*>,
    invokeOnEmpty: Boolean = false,
    lineCallback: (String) -> Unit,
): Int {
    val sink = object : AllLinesSink {
        override fun handle(handler: ProcessHandler, line: String) {
            if (invokeOnEmpty || line.isNotEmpty()) lineCallback(line)
        }
    }

    val exitCode = launchProcess(*args, config = config, sink = sink)
    return exitCode
}
