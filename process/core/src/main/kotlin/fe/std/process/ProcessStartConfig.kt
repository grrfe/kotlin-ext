package fe.std.process

public interface ProcessStartConfig<out T : ProcessStatus> {
    public fun start(process: Process, runnable: Runnable): T
}

public interface ProcessStatus

public data object Config : ProcessStatus
public interface Started : ProcessStatus
public data class Stopped(val exitCode: Int) : ProcessStatus

