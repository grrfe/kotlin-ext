package fe.std.process.sink

import fe.std.process.ProcessHandler

public sealed interface LineSink : Disposable {
    public fun receive(handler: ProcessHandler, line: String): Boolean
}

public interface AllLinesSink : LineSink {
    public fun handle(handler: ProcessHandler, line: String)

    override fun receive(handler: ProcessHandler, line: String): Boolean {
        handle(handler, line)
        return false
    }

    override fun dispose() {}
}

public interface ResultSink : LineSink {
    override fun receive(handler: ProcessHandler, line: String): Boolean

    override fun dispose() {}
}
