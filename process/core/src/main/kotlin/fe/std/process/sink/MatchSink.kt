package fe.std.process.sink

import fe.std.process.ProcessHandler

public abstract class LineContainsSink(private val string: String) : ResultSink {

    public abstract fun handle(handler: ProcessHandler, line: String, index: Int)

    override fun receive(handler: ProcessHandler, line: String): Boolean {
        val index = line.indexOf(string)

        val found = index != -1
        if (found) handle(handler, line, index)
        return found
    }
}

public abstract class LineRegexMatchSink(
    private val regex: Regex,
    private val containsString: String? = null,
) : ResultSink {

    public abstract fun handle(handler: ProcessHandler, matchResult: MatchResult)

    override fun receive(handler: ProcessHandler, line: String): Boolean {
        val contains = containsString == null || line.contains(containsString)
        val matchResult = if (contains) regex.matchEntire(line) else null

        val found = matchResult != null
        if (found) handle(handler, matchResult)
        return found
    }
}
