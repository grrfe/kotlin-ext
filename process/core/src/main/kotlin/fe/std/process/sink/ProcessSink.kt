package fe.std.process.sink;

import fe.std.process.ProcessHandler
import fe.std.process.ProcessStatus

public sealed interface ProcessSink

public interface Disposable : ProcessSink {
    public fun dispose()
}

public fun interface StatusChanged : ProcessSink {
    public fun changed(handler: ProcessHandler, oldStatus: ProcessStatus, newStatus: ProcessStatus)
}
