package fe.std.process.sink

import fe.std.process.ProcessHandler
import java.io.File
import java.io.OutputStream
import java.io.PrintWriter

public abstract class WriteSink(outputStream: OutputStream) : AllLinesSink {
    private val writer = PrintWriter(outputStream)

    override fun handle(handler: ProcessHandler, line: String) {
        writer.println(line)
        writer.flush()
    }

    override fun dispose() {
        writer.close()
    }

    public class FileWriter(file: File) : WriteSink(file.outputStream())
}
