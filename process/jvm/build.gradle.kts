plugins {
    kotlin("jvm")
}

dependencies {
    api(project(":process-core"))
    testImplementation(kotlin("test"))
}
