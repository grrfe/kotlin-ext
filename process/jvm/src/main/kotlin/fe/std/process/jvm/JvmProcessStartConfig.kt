package fe.std.process.jvm

import fe.std.process.ProcessStartConfig
import fe.std.process.Started
import java.lang.Process
import java.lang.Runnable

public data object Standalone : ProcessStartConfig<JvmStarted.Standalone> {
    override fun start(process: Process, runnable: Runnable): JvmStarted.Standalone {
        runnable.run()
        return JvmStarted.Standalone(process.pid())
    }
}

public abstract class ThreadedProcess : ProcessStartConfig<JvmStarted.ThreadedProcess> {
    public companion object Default : ThreadedProcess() {
        override fun start(process: Process, runnable: Runnable): JvmStarted.ThreadedProcess {
            val thread = Thread(runnable)
            thread.start()

            return JvmStarted.ThreadedProcess(process.pid(), thread)
        }
    }
}

public abstract class JvmStarted(public val pid: Long) : Started {
    public class Standalone(pid: Long) : JvmStarted(pid)
    public class ThreadedProcess(pid: Long, public val thread: Thread) : JvmStarted(pid)
}
