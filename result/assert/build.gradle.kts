plugins {
    kotlin("jvm")
}

dependencies {
    api(project(":result-core"))
    implementation("com.willowtreeapps.assertk:assertk:_")
}
