package fe.std.result.assert

import assertk.Assert
import assertk.assertions.isInstanceOf
import fe.std.result.Failure
import fe.std.result.IResult
import fe.std.result.Success

public fun <T> assertSuccess(result: IResult<T>): Assert<T> {
    return assertk.assertThat(result)
        .isInstanceOf<Success<T>>()
        .transform { it.value }
}

public fun <T> assertFailure(result: IResult<T>): Assert<Throwable> {
    return assertk.assertThat(result)
        .isInstanceOf<Failure<T>>()
        .transform { it.exception }
}
