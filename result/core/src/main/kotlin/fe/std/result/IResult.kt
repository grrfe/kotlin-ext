package fe.std.result

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract
import kotlin.reflect.KProperty

public sealed interface IResult<T>

@JvmInline
public value class Success<T>(public val value: T) : IResult<T>

public operator fun <T> Success<T>.getValue(t: T, property: KProperty<*>): T {
    return value
}

@JvmInline
public value class Failure<T>(public val exception: Throwable = DummyException) : IResult<T> {
    public fun <R> cast(): Failure<R> {
        @Suppress("UNCHECKED_CAST")
        return this as Failure<R>
    }
}

public data object DummyException : Throwable("Result dummy exception") {
    private fun readResolve(): Any = DummyException
}

public operator fun <T, R> Failure<T>.unaryPlus(): Failure<R> {
    return cast()
}

@OptIn(ExperimentalContracts::class)
public fun <T> IResult<T>.isFailure(): Boolean {
    contract {
        returns(true) implies (this@isFailure is Failure)
        returns(false) implies (this@isFailure is Success)
    }

    return this is Failure<T>
}

@OptIn(ExperimentalContracts::class)
public fun <T> IResult<T>.isSuccess(): Boolean {
    contract {
        returns(true) implies (this@isSuccess is Success)
        returns(false) implies (this@isSuccess is Failure)
    }

    return this is Success<T>
}

@OptIn(ExperimentalContracts::class)
public fun <T> IResult<T>.ifFailure(): Failure<T>? {
    contract {
        returnsNotNull() implies (this@ifFailure is Failure)
    }

    return this as? Failure<T>
}

@OptIn(ExperimentalContracts::class)
public fun <T> IResult<T>.ifSuccess(): Success<T>? {
    contract {
        returnsNotNull() implies (this@ifSuccess is Success)
    }

    return this as? Success<T>
}

@OptIn(ExperimentalContracts::class)
public fun <T> IResult<T>.getOrNull(): T? {
    contract {
        returnsNotNull() implies (this@getOrNull is Success)
    }

    return when (this) {
        is Success -> value
        is Failure -> null
    }
}

@OptIn(ExperimentalContracts::class)
public fun <T> IResult<T>.mapFailure(block: (Throwable) -> Throwable): IResult<T> {
    contract {
        callsInPlace(block, InvocationKind.AT_MOST_ONCE)
    }

    return when (this) {
        is Success -> this
        is Failure -> Failure(block(exception))
    }
}

public inline fun <R> tryCatch(block: () -> R): IResult<R> {
    return try {
        Success(block())
    } catch (e: Throwable) {
        Failure(e)
    }
}

@Deprecated("Use toExtResult() instead", replaceWith = ReplaceWith("toExtResult()"))
public fun <T> Result<T>.wrap(): IResult<T> {
    return toExtResult()
}

public fun <T> Result<T>.toExtResult(): IResult<T> {
    return when (val value = getOrNull()) {
        null -> Failure(exceptionOrNull()!!)
        else -> Success(value)
    }
}

public fun <T> T.asSuccess(): Success<T> {
    return Success(this)
}

public val <T> T.success: Success<T>
    get() = Success(this)
