package fe.std.result

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class ResultTest {
    @Test
    fun `expect success`() {
        val result = tryCatch { Unit }
        assertTrue(result.isSuccess())
        assertEquals(Unit, result.value)
    }

    @Test
    fun `expect failure`() {
        val result = tryCatch { throw Exception("Fail") }
        assertTrue(result.isFailure())
        assertEquals("Fail", result.exception.message)
    }
}
