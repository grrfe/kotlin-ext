import fe.buildsettings.extension.includeProject
import fe.buildsettings.extension.maybeResolveIncludingRootContext

rootProject.name = "kotlin-ext"

pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
        maven { url = uri("https://jitpack.io") }
    }

    plugins {
        id("de.fayard.refreshVersions") version "0.60.5"
        id("net.nemerosa.versioning") version "3.1.0"
        kotlin("jvm")
    }

    when(val gradleBuildDir = extra.properties["gradle.build.dir"]) {
        null -> {
            val gradleBuildVersion = extra.properties["gradle.build.version"]
            val plugins = mapOf(
                "com.gitlab.grrfe.build-settings-plugin" to "com.gitlab.grrfe.gradle-build:build-settings",
                "com.gitlab.grrfe.build-logic-plugin" to "com.gitlab.grrfe.gradle-build:build-logic"
            )

            resolutionStrategy {
                eachPlugin {
                    plugins[requested.id.id]?.let { useModule("$it:$gradleBuildVersion") }
                }
            }
        }
        else -> includeBuild(gradleBuildDir.toString())
    }
}

plugins {
    id("de.fayard.refreshVersions")
    id("com.gitlab.grrfe.build-settings-plugin")
}

extra.properties["gradle.build.dir"]
    ?.let { includeBuild(it.toString()) }

maybeResolveIncludingRootContext()?.rootProject {
    refreshVersions {
        versionsPropertiesFile = rootDir.resolve("versions.properties")
        logger.info("Using versions file from $versionsPropertiesFile")
    }
}

includeProject(":core", "core")
includeProject(":dom-ext", "dom-ext")

//include(":io-core", "io/core")
includeProject(":io-jvm", "io/jvm")
includeProject(":io-kotlinx-core", "io/kotlinx/core")
includeProject(":io-kotlinx-test", "io/kotlinx/test")

includeProject(":test", "test")
includeProject(":uri", "uri")
includeProject(":stringbuilder", "stringbuilder")
includeProject(":benchmark", "benchmark")

includeProject(":time-core", "time/core")
includeProject(":time-java", "time/java")

includeProject(":result-core", "result/core")
includeProject(":result-assert", "result/assert")

includeProject(":process-core", "process/core")
includeProject(":process-jvm", "process/jvm")

include(":platform")

