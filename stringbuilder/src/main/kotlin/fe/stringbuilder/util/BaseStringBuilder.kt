package fe.stringbuilder.util


public interface BaseAppendable<T : Appendable, B> {
    public fun apply(appendable: T, block: B)

    public fun build(appendable: T, block: B): T {
        apply(appendable, block)
        return appendable
    }
}
