package fe.stringbuilder.util

public typealias BuilderScope<T> = T.() -> Unit
