package fe.stringbuilder.util


public class SeparatedStringBuilder<T : Appendable>(
    private val separator: String,
    private val items: MutableList<T.() -> Unit> = mutableListOf()
) : BaseAppendable<T, BuilderScope<SeparatedStringBuilder<T>>?> {
    @Deprecated("Use item(prefix, block) instead", replaceWith = ReplaceWith("item(prefix, block)"))
    public fun item(block: T.() -> Unit): Boolean = items.add(block)

    @Suppress("MemberVisibilityCanBePrivate")
    public fun item(prefix: String? = null, block: T.() -> Unit): SeparatedStringBuilder<T> {
        items.add { append(prefix) }
        items.add(block)

        return this
    }

    @Deprecated(
        "Use itemNotNull(predicate, prefix, block) instead",
        replaceWith = ReplaceWith("itemNotNull(predicate, prefix, block)")
    )
    public fun <P> itemNotNull(predicate: P?, block: T.() -> Unit) {
        if (predicate != null) item(block)
    }

    public fun <P> itemNotNull(predicate: P?, prefix: String? = null, block: T.() -> Unit): SeparatedStringBuilder<T> {
        if (predicate != null) {
            item(prefix, block)
        }

        return this
    }

    public fun items(blocks: Array<out Appendable.() -> Unit>): Boolean = items.addAll(blocks)

    override fun apply(appendable: T, block: (SeparatedStringBuilder<T>.() -> Unit)?) {
        if (block != null) {
            block()
        }

        for ((index, element) in items.withIndex()) {
            val last = index + 1 == items.size

            appendable.apply(element)
            if (!last) appendable.append(separator)
        }
    }
}

public fun buildSeparatedString(
    separator: String,
    builder: BuilderScope<SeparatedStringBuilder<StringBuilder>>
): String {
    return StringBuilder().separated(separator, builder).toString()
}
