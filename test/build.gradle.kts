plugins {
    kotlin("jvm")
}
dependencies {
    implementation("com.willowtreeapps.assertk:assertk:_")
    testImplementation(kotlin("test"))
}
