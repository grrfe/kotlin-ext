package fe.std.assert

import assertk.assertAll


public inline fun <T> assertEach(iterable: Iterable<T>, block: (T) -> Unit) {
    assertAll {
        for (element in iterable) {
            block(element)
        }
    }
}

