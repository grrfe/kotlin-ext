package fe.std.test


public fun String.trimMargin(lineSeparator: String = "\n", marginPrefix: String = "|"): String {
    return replaceIndentByMargin(lineSeparator, "", marginPrefix)
}

/**
 * Detects indent by [marginPrefix] as it does [trimMargin] and replace it with [newIndent].
 *
 * The lines in the original string can be separated with `\r\n` (CRLF), `\n` (LF), or `\r` (CR) characters, however, the lines in the
 * resulting string will be separated with [lineSeparator]
 *.
 * @param lineSeparator separator to join together the resulting string. Default is `\n` (LF)
 * @param marginPrefix non-blank string, which is used as a margin delimiter. Default is `|` (pipe character).
 */
public fun String.replaceIndentByMargin(lineSeparator: String = "\n", newIndent: String = "", marginPrefix: String = "|"): String {
    require(marginPrefix.isNotBlank()) { "marginPrefix must be non-blank string." }
    val lines = lines()

    return lines.reindent(lineSeparator, length + newIndent.length * lines.size, getIndentFunction(newIndent)) { line ->
        val firstNonWhitespaceIndex = line.indexOfFirst { !it.isWhitespace() }

        when {
            firstNonWhitespaceIndex == -1 -> null
            line.startsWith(
                marginPrefix,
                firstNonWhitespaceIndex
            ) -> line.substring(firstNonWhitespaceIndex + marginPrefix.length)

            else -> null
        }
    }
}

private fun getIndentFunction(indent: String) = when {
    indent.isEmpty() -> { line: String -> line }
    else -> { line: String -> indent + line }
}

private inline fun List<String>.reindent(
    lineSeparator: String,
    resultSizeEstimate: Int,
    indentAddFunction: (String) -> String,
    indentCutFunction: (String) -> String?,
): String {
    val lastIndex = lastIndex
    return mapIndexedNotNull { index, value ->
        if ((index == 0 || index == lastIndex) && value.isBlank())
            null
        else
            indentCutFunction(value)?.let(indentAddFunction) ?: value
    }
        .joinTo(StringBuilder(resultSizeEstimate), lineSeparator)
        .toString()
}
