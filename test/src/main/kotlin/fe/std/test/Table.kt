package fe.std.test

import assertk.Table2
import assertk.Table3
import assertk.Table4
import assertk.tableOf

public fun <C1> tableTest(column1: String): Table1Builder<C1> {
    return Table1Builder(column1)
}

public fun <C1, C2> tableTest(column1: String, column2: String): Table2Builder<C1, C2> {
    return Table2Builder(column1, column2)
}

public fun <C1, C2, C3> tableTest(column1: String, column2: String, column3: String): Table3Builder<C1, C2, C3> {
    return Table3Builder(column1, column2, column3)
}

public fun interface TestFunction<I, R> {
    public fun run(input: I): R
}

public abstract class TableBuilder<Row>(
    initialRows: List<Row> = emptyList(),
) {
    public val rows: MutableList<Row> = initialRows.toMutableList()

    public fun addRow(row: Row) {
        rows.add(row)
    }
}

public class Table1Builder<C1>(
    public val column1: String,
) : TableBuilder<Row1<C1>>(

) {
    public fun row(column1: C1): Table1Builder<C1> {
        rows.add(Row1(column1))
        return this
    }

    public fun <C2> prepare(column2: String, fn: (C1) -> C2): Table2Builder<C1, C2> {
        val builder = Table2Builder<C1, C2>(column1, column2)

        for (row in rows) {
            val (c1) = row
            builder.addRow(Row2(row.column1, fn(c1)))
        }

        return builder
    }

    public fun <I, R> test(testFunction: TestFunction<I, R>): Table2<TestFunction<I, R>, C1> {
        require(rows.isNotEmpty()) { "At least one row is required" }

        val tableBuilder = tableOf("test", column1)
        val row = rows[0]
        var table = tableBuilder.row(testFunction, row.column1)

        for (i in 1 until rows.size) {
            val row = rows[i]
            table = table.row(testFunction, row.column1)
        }

        return table
    }
}

public class Table2Builder<C1, C2>(
    public val column1: String,
    public val column2: String
) : TableBuilder<Row2<C1, C2>>(

) {
    public fun row(column1: C1, column2: C2): Table2Builder<C1, C2> {
        rows.add(Row2(column1, column2))
        return this
    }

    public fun <C3> prepare(column3: String, fn: (C1, C2) -> C3): Table3Builder<C1, C2, C3> {
        val builder = Table3Builder<C1, C2, C3>(column1, column2, column3)

        for (row in rows) {
            val (c1, c2) = row
            builder.addRow(Row3(row.column1, row.column2, fn(c1, c2)))
        }

        return builder
    }

    public fun <I, R> test(testFunction: TestFunction<I, R>): Table3<TestFunction<I, R>, C1, C2> {
        require(rows.isNotEmpty()) { "At least one row is required" }

        val tableBuilder = tableOf("test", column1, column2)
        val row = rows[0]
        var table = tableBuilder.row(testFunction, row.column1, row.column2)

        for (i in 1 until rows.size) {
            val row = rows[i]
            table = table.row(testFunction, row.column1, row.column2)
        }

        return table
    }
}

public class Table3Builder<C1, C2, C3>(
    public val column1: String,
    public val column2: String,
    public val column3: String,
) : TableBuilder<Row3<C1, C2, C3>>() {
    public fun row(column1: C1, column2: C2, column3: C3): Table3Builder<C1, C2, C3> {
        rows.add(Row3(column1, column2, column3))
        return this
    }

    public fun <I, R> test(testFunction: TestFunction<I, R>): Table4<TestFunction<I, R>, C1, C2, C3> {
        require(rows.isNotEmpty()) { "At least one row is required" }

        val tableBuilder = tableOf("test", column1, column2, column3)
        val row = rows[0]
        var table = tableBuilder.row(testFunction, row.column1, row.column2, row.column3)

        for (i in 1 until rows.size) {
            val row = rows[i]
            table = table.row(testFunction, row.column1, row.column2, row.column3)
        }

        return table
    }
}


public data class Row1<C1>(val column1: C1)
public data class Row2<C1, C2>(val column1: C1, val column2: C2)
public data class Row3<C1, C2, C3>(val column1: C1, val column2: C2, val column3: C3)
//public data class Row4<C1, C2, C3, C4>(val column1: C1, val column2: C2, val column3: C3, val column4: C4)

public class TestCase {

}
