package fe.std.test

import assertk.assertThat
import assertk.assertions.isTrue
import kotlin.test.Test

internal class TableTest {
    @Test
    fun test() {
        tableTest<String, String>("input", "input 2")
            .row("hello", "world")
            .row("foo", "bar")
            .prepare(column3 = "prepared") { input, input2 -> input + input2 }
            .test<String, Boolean> { it.isNotEmpty() }
            .forAll { test, _, _, prepared ->
                assertThat(test.run(prepared)).isTrue()
            }
    }
}
