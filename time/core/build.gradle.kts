plugins {
    kotlin("jvm")
}
dependencies {
    testImplementation("com.willowtreeapps.assertk:assertk:_")
    testImplementation(kotlin("test"))
}
