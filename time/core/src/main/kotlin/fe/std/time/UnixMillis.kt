package fe.std.time


private val monthDays = intArrayOf(0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334)

//shamelessly stolen from https://stackoverflow.com/a/57744744
public fun unixMillisOf(
    year: Int = 1970,
    month: Int = 1,
    dayOfMonth: Int = 1,
    hour: Int = 0,
    minute: Int = 0,
    second: Int = 0,
    millisecond: Int = 0
): Long {
    require(year >= 1970)
    require(month in 1..12)
    require(dayOfMonth in 1..31)
    require(hour in 0..23)
    require(minute in 0..59)
    require(second in 0..59)
    require(millisecond in 0..999)

    // Detect potential leap day (February 29th) in this year?
    val minusYear = if (month >= 3) 1 else 0
    // Compute one year less in the non-leap years sum
    val yearsSince1970 = year - 1970 + minusYear

    // + days in previous months (leap day not included)
    val daysInPrevMonths = monthDays[month - 1]

    // Add a day for each year divisible by 4 starting from 1973
    val daysDivisible4 = (yearsSince1970 + 1) / 4
    // - days for each year divisible by 100 (starting from 2001)
    val daysDivisible100 = (yearsSince1970 + 69) / 100
    // + days for each year divisible by 400 (starting from 2001)
    val daysDivisible400 = (yearsSince1970 + 369) / 400
    // + days for each year (as all are non-leap years) from 1970 (minus this year if potential leap day taken into account)
    val daysYear = 365 * (yearsSince1970 - minusYear)
//        (5 * 73 /*=365*/)

    // + Day (zero index)
    val day = dayOfMonth - 1 + daysInPrevMonths + daysDivisible4 - daysDivisible100 + daysDivisible400 + daysYear

    // + Hours from computed days
    val hours = 24 * day + hour
    // + Minutes from computed hours
    val minutes = 60 * hours + minute
    // + Seconds from computed minutes
    val seconds = 60 * minutes + second
    val millis = 1000L * seconds + millisecond

    return millis
}
