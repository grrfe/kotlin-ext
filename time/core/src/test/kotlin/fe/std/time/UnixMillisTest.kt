package fe.std.time

import assertk.assertThat
import assertk.assertions.isEqualTo
import kotlin.test.Test

internal class UnixMillisTest {
    @Test
    fun test() {
        assertThat(unixMillisOf(1970, 1, 1, 0, 0, 0, 0)).isEqualTo(0)
        assertThat(unixMillisOf(2004, 9, 16, 23, 59, 59, 750)).isEqualTo(1095379199750)
        assertThat(unixMillisOf(2004, 9, 17, 0, 0, 0, 0)).isEqualTo(1095379200000)
    }
}
