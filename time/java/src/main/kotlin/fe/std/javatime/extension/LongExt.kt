package fe.std.javatime.extension

import fe.std.javatime.time.Timezone
import fe.std.javatime.time.UnixMillis
import fe.std.javatime.time.WithZone
import fe.std.javatime.time.WithoutZone
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

public val Long.unixMillisUtc: UnixMillis<LocalDateTime>
    get() = WithoutZone(this)

public fun Long.unixMillisAtZone(zoneId: ZoneId = Timezone.Utc): UnixMillis<ZonedDateTime> {
    return WithZone(this, zoneId)
}
