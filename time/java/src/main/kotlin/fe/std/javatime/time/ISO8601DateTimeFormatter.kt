package fe.std.javatime.time

import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField
import java.time.temporal.Temporal
import java.util.*

public enum class ISO8601DateTimeFormatter {
    ReplaceTWithSpace,
    DisableNanoSeconds,
    DisableSeconds,
    EnableTimeZoneOffset;

    public companion object {
        /**
         * The default format is yyyy-MM-dd HH:mm:ss
         */
        @Suppress("MemberVisibilityCanBePrivate")
        public val DefaultFormat: DateTimeFormatter by lazy { buildFormat() }

        public fun <T : Temporal> format(
            unixMillis: Long,
            format: DateTimeFormatter = DefaultFormat,
            obtain: (Long) -> UnixMillis<T>
        ): String {
            return obtain(unixMillis).format(format)
        }

        @Suppress("MemberVisibilityCanBePrivate")
        public fun buildFormat(
            locale: Locale = Locale.getDefault(Locale.Category.FORMAT),
            options: EnumSet<ISO8601DateTimeFormatter> = EnumSet.of(ReplaceTWithSpace, DisableNanoSeconds),
        ): DateTimeFormatter {
            val builder = DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                .append(DateTimeFormatter.ISO_LOCAL_DATE)
                .appendLiteral(if (ReplaceTWithSpace in options) " " else "T")
                .appendValue(ChronoField.HOUR_OF_DAY, 2)
                .appendLiteral(':')
                .appendValue(ChronoField.MINUTE_OF_HOUR, 2)

            if (DisableSeconds !in options) {
                builder.optionalStart()
                    .appendLiteral(':')
                    .appendValue(ChronoField.SECOND_OF_MINUTE, 2)
            }

            if (DisableNanoSeconds !in options) {
                builder.optionalStart().appendFraction(ChronoField.NANO_OF_SECOND, 3, 9, true)
            }

            if (EnableTimeZoneOffset in options) {
                builder.appendOffsetId()
            }

            return builder.toFormatter(locale)
        }
    }
}
