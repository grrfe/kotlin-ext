package fe.std.javatime.time

import java.time.LocalDate
import java.time.LocalDateTime

public fun LocalDate.atEndOfDay(): LocalDateTime {
    return atTime(23, 59, 59, 999_999_999)
}
