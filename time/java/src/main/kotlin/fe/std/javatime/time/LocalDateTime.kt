package fe.std.javatime.time

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.Locale

public fun LocalDateTime.localizedString(
    locale: Locale = Locale.getDefault(),
    formatStyle: FormatStyle = FormatStyle.MEDIUM,
    zone: ZoneId = Timezone.SystemDefault,
): String {
    return format(DateTimeFormatter.ofLocalizedDateTime(formatStyle).withZone(zone).withLocale(locale))
}

public val LocalDateTime.unixMillis: UnixMillis<LocalDateTime>
    get() = FromZoneless(this)
