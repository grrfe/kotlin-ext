package fe.std.javatime.time

import java.time.ZoneId

public object Timezone {
    public val Utc: ZoneId = ZoneId.of("UTC")

    public val SystemDefault: ZoneId
        get() = ZoneId.systemDefault()
}
