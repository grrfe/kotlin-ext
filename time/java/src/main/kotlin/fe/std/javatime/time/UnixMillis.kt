package fe.std.javatime.time

import java.time.Instant
import java.time.format.DateTimeFormatter
import java.time.temporal.Temporal
import kotlin.reflect.KProperty

public sealed class UnixMillis<T : Temporal>(
    public val millis: Long,
    public val instant: Instant = Instant.ofEpochMilli(tryFix(millis))
) {
    protected constructor(instant: Instant) : this(instant.toEpochMilli(), instant)

    public companion object {
        private fun tryFix(millis: Long): Long {
            // If user passes seconds, convert them to millis for them
            return if (millis / 100_000_000_000 >= 1) millis else millis * 1_000
        }
    }

    public val value: T by lazy { getTemporal() }

    protected abstract fun getTemporal(): T

    protected abstract fun formatInternal(dateTimeFormatter: DateTimeFormatter): String

    public fun format(dateTimeFormatter: DateTimeFormatter): String {
        return formatInternal(dateTimeFormatter)
    }

    public operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return value
    }
}
