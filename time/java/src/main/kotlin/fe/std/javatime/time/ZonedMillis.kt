package fe.std.javatime.time

import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


public class WithZone(millis: Long, private val zone: ZoneId) : UnixMillis<ZonedDateTime>(millis) {
    override fun getTemporal(): ZonedDateTime {
        return ZonedDateTime.ofInstant(instant, zone)
    }

    override fun formatInternal(dateTimeFormatter: DateTimeFormatter): String {
        return value.format(dateTimeFormatter)
    }
}

public class FromZoned(
    private val zonedDateTime: ZonedDateTime,
) : UnixMillis<ZonedDateTime>(zonedDateTime.toInstant()) {

    override fun getTemporal(): ZonedDateTime {
        return zonedDateTime
    }

    override fun formatInternal(dateTimeFormatter: DateTimeFormatter): String {
        return value.format(dateTimeFormatter)
    }
}
