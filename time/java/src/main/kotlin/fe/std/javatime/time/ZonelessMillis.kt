package fe.std.javatime.time

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

public class FromZoneless(
    private val localDateTime: LocalDateTime,
) : UnixMillis<LocalDateTime>(localDateTime.atZone(Timezone.SystemDefault).toInstant()) {

    override fun getTemporal(): LocalDateTime {
        return localDateTime
    }

    override fun formatInternal(dateTimeFormatter: DateTimeFormatter): String {
        return value.format(dateTimeFormatter)
    }
}

public class WithoutZone(millis: Long) : UnixMillis<LocalDateTime>(millis) {
    // Theoretically, the system default zone could change between instantiation and the first call to getTemporal,
    // therefore we make sure to store the zone at instantiation
    private val zoneId = Timezone.SystemDefault

    override fun getTemporal(): LocalDateTime {
        return LocalDateTime.ofInstant(instant, zoneId)
    }

    override fun formatInternal(dateTimeFormatter: DateTimeFormatter): String {
        return value.format(dateTimeFormatter)
    }
}
