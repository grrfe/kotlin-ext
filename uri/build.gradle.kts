plugins {
    kotlin("jvm")
}

dependencies {
    api(project(":result-core"))
    testImplementation("com.willowtreeapps.assertk:assertk:_")
    testImplementation(kotlin("test"))
}
