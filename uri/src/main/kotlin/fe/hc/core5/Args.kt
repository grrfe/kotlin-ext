package fe.hc.core5

import fe.hc.core5.TextUtils.containsBlanks

public object Args {
    public fun check(expression: Boolean, message: String) {
        require(expression) { message }
    }

    public fun check(expression: Boolean, message: String, vararg args: Any) {
        require(expression) { message.format(*args) }
    }

    public fun check(expression: Boolean, message: String, arg: Any) {
        require(expression) { message.format(arg) }
    }

    public fun notNegative(value: Long, name: String): Long {
        require(value >= 0) {
            "%s must not be negative: %d".format(name, value)
        }

        return value
    }

    public fun notNegative(value: Int, name: String): Int {
        require(value >= 0) {
            "%s must not be negative: %d".format(name, value)
        }

        return value
    }

    public fun checkRange(
        value: Int,
        lowInclusive: Int,
        highInclusive: Int,
        message: String?,
    ) {
        require(value in lowInclusive..highInclusive) {
            "%s: %d is out of range [%d, %d]".format(message, value, lowInclusive, highInclusive)
        }
    }

    public fun checkRange(
        value: Long,
        lowInclusive: Long,
        highInclusive: Long,
        message: String?,
    ) {
        require(value in lowInclusive..highInclusive) {
            "%s: %d is out of range [%d, %d]".format(message, value, lowInclusive, highInclusive)
        }
    }

    public fun <T : CharSequence?> containsNoBlanks(argument: T?, name: String): T {
        requireNotNull(argument)
        require(argument.isNotEmpty()) { name }
        require(!argument.containsBlanks()) { "$name must not contain blanks" }

        return argument
    }

    public fun <T : CharSequence?> notEmpty(argument: T?, name: String): T {
        requireNotNull(argument)
        require(argument.isNotEmpty()) { name }

        return argument
    }


    public fun <T : CharSequence?> notBlank(argument: T?, name: String): T {
        requireNotNull(argument)
        require(argument.isNotBlank()) { "$name must not be blank" }

        return argument
    }
}
