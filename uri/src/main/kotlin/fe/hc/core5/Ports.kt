package fe.hc.core5

import fe.hc.core5.Ports.Companion.MAX_VALUE
import fe.hc.core5.Ports.Companion.SCHEME_DEFAULT

@JvmInline
public value class Ports(public val value: Int) {
    init {
        require(value in SCHEME_DEFAULT..MAX_VALUE)
    }

    public companion object {
        public const val SCHEME_DEFAULT: Int = -1
        public const val MIN_VALUE: Int = 0
        public const val MAX_VALUE: Int = 65535

        public val SchemeDefault: Ports = Ports(SCHEME_DEFAULT)
    }
}

public fun checkValid(value: Int): Int {
    require(value in SCHEME_DEFAULT..MAX_VALUE)
    return value
}
