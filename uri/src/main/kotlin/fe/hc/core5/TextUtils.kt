package fe.hc.core5



public object TextUtils {
    public fun isEmpty(s: CharSequence): Boolean {
        return s.isEmpty()
    }

    public fun isBlank(s: CharSequence?): Boolean {
        return s?.isBlank() != false
    }

    public fun CharSequence.containsBlanks(): Boolean {
        return any { it.isWhitespace() }
    }

    public fun length(cs: CharSequence?): Int {
        return cs?.length ?: 0
    }

    @OptIn(ExperimentalStdlibApi::class)
    public fun toHexString(bytes: ByteArray): String {
        return bytes.toHexString()
    }

    public fun toLowerCase(s: String?): String? {
        return s?.lowercase()
    }

    public fun isAllASCII(s: CharSequence): Boolean {
        for (c in s) {
            if (c.code > 0x7F) {
                return false
            }
        }

        return true
    }
}
