package fe.hc.core5

public enum class URIScheme(public val scheme: String) {
    HTTP("http"), HTTPS("https");

    public fun same(scheme: String?): Boolean {
        return scheme.equals(scheme, ignoreCase = true)
    }

    override fun toString(): String {
        return scheme
    }
}
