package fe.std.uri

import fe.relocated.org.apache.hc.core5.core5.net.URIBuilder
import java.net.URI
import java.nio.charset.Charset

public sealed interface UriParseResult

public class ParserFailure(public val exception: Throwable) : UriParseResult

@ConsistentCopyVisibility
public data class Url internal constructor(
    val scheme: String?,
    val encodedSchemeSpecificPart: String?,
    val encodedAuthority: String?,
    val uri: URI,
    val host: String?,
    val port: Int,
    val encodedUserInfo: String?,
    val userInfo: String?,
    val encodedPath: String?,
    val pathSegments: List<String>,
    val pathRootless: Boolean,
    val encodedQuery: String?,
    val queryParams: List<Pair<String, String?>>,
    val encodedFragment: String?,
    val fragment: String?,
    val charset: Charset,
) : UriParseResult {
    public val isPathEmpty: Boolean by lazy { pathSegments.isEmpty() && encodedPath.isNullOrEmpty() }
    public val isQueryEmpty: Boolean by lazy { queryParams.isEmpty() && encodedQuery == null }

    public val pathString: String by lazy {
        buildString {
            for (segment in pathSegments) {
                append('/').append(segment)
            }
        }
    }

    public fun getFirstQueryParam(name: String): Pair<String, String?>? {
        return queryParams.firstOrNull { name == it.first }
    }

    override fun toString(): String {
        return uri.toString()
    }

    public val formattedQuery: String by lazy {
        buildString {
            URIBuilder.formatQuery(this, queryParams, charset, false)
        }
    }
}
