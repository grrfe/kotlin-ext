package fe.std.uri

import fe.hc.core5.URIScheme
import fe.relocated.org.apache.hc.core5.core5.net.PercentCodec
import fe.relocated.org.apache.hc.core5.core5.net.URIBuilder
import fe.std.result.IResult
import fe.std.result.asSuccess
import fe.std.result.getOrNull
import fe.std.result.isSuccess
import fe.std.result.tryCatch
import java.net.URI

public object UrlFactory {
    private fun build(url: String, setPlusAsBlanks: Boolean): Pair<URIBuilder, URI> {
        val baseUri = URI(url)
        val builder = URIBuilder(baseUri).setPlusAsBlank(setPlusAsBlanks)

        val uri = builder.build()
        return builder to uri
    }

    public fun createUrl(builder: URIBuilder, uri: URI): Url {
        return Url(
            builder.scheme,
            builder.encodedSchemeSpecificPart,
            builder.encodedAuthority,
            uri,
            builder.host,
            builder.port,
            builder.encodedUserInfo,
            builder.userInfo,
            builder.encodedPath,
            builder.pathSegments,
            builder.isPathRootless,
            builder.encodedQuery,
            builder.queryParams,
            builder.encodedFragment,
            builder.fragment,
            builder.charset
        )
    }

    private fun applyModifications(
        url: String,
        sanitizeFragment: Boolean = true,
        fallbackScheme: URIScheme? = URIScheme.HTTP,
    ): String {
        var mutUrl = url

        mutUrl = if (sanitizeFragment) sanitizeFragment(mutUrl) else mutUrl
        mutUrl = if (fallbackScheme != null) fixHttpUrl(mutUrl, fallbackScheme) ?: mutUrl else mutUrl

        return mutUrl
    }

    public fun parse(
        url: String,
        sanitizeFragment: Boolean = true,
        setPlusAsBlanks: Boolean = false,
        fallbackScheme: URIScheme? = URIScheme.HTTPS,
    ): IResult<Url> {
        val result = tryCatch {
            build(applyModifications(url, sanitizeFragment, fallbackScheme), setPlusAsBlanks)
        }

        return when (result.isSuccess()) {
            false -> result.cast()
            true -> {
                val (builder, uri) = result.value
                createUrl(builder, uri).asSuccess()
            }
        }
    }

    private const val fragment = "#"

    public fun sanitizeFragment(url: String): String {
        val firstHashIndex = url.indexOf(fragment)

        if (firstHashIndex > -1 && url.indexOf(fragment, firstHashIndex + 1) > -1) {
            val preFragment = url.substring(0, firstHashIndex)
            val fragmentContent = url.substring(firstHashIndex + 1)

            return preFragment + fragment + PercentCodec.encode(fragmentContent, Charsets.UTF_8)
        }

        return url
    }

    // e.g. file:, http:, ftp:
    private const val uriSchemeDivider = ":"
    private const val httpSchemeSpecificPart = "//"

    @Deprecated(
        message = "Use fixHttpUrl(url, fallbackScheme)",
        replaceWith = ReplaceWith("fixHttpUrl(url, fallbackScheme)")
    )
    public fun fixHttpUrlFast(url: String, fallbackScheme: URIScheme = URIScheme.HTTPS): String? {
        return fixHttpUrl(url, fallbackScheme)
    }

    public fun fixHttpUrl(url: String, fallbackScheme: URIScheme = URIScheme.HTTPS): String? {
        if (url.isEmpty()) return null

        val dividerIdx = url.indexOf(uriSchemeDivider)
        val httpSchemeIdx = url.indexOf(httpSchemeSpecificPart)

        if(httpSchemeIdx == 0) {
            return "${fallbackScheme.scheme}$uriSchemeDivider$url"
        }

        if (dividerIdx == -1 && httpSchemeIdx == -1) {
            return "${fallbackScheme.scheme}$uriSchemeDivider$httpSchemeSpecificPart$url"
        }

        if (url.startsWith("http://") || url.startsWith("https://")) {
            if (httpSchemeIdx != dividerIdx + 1) return null
            if (httpSchemeIdx + httpSchemeSpecificPart.length >= url.length) return null
            return url
        }

        return null
    }
}

public fun Url(
    url: String,
    sanitizeFragment: Boolean = true,
    setPlusAsBlanks: Boolean = false,
    fallbackScheme: URIScheme? = URIScheme.HTTPS,
): UriParseResult {
    val result = UrlFactory.parse(url, sanitizeFragment, setPlusAsBlanks, fallbackScheme)
    return when (result.isSuccess()) {
        false -> ParserFailure(result.exception)
        true -> result.value
    }
}


public fun String.toUrl(
    sanitizeFragment: Boolean = true,
    setPlusAsBlanks: Boolean = false,
    fallbackScheme: URIScheme? = URIScheme.HTTPS,
): IResult<Url> {
    return UrlFactory.parse(this, sanitizeFragment, setPlusAsBlanks, fallbackScheme)
}

public fun String.toUrlOrNull(
    sanitizeFragment: Boolean = true,
    setPlusAsBlanks: Boolean = false,
    fallbackScheme: URIScheme? = URIScheme.HTTPS,
): Url? {
    return toUrl(sanitizeFragment, setPlusAsBlanks, fallbackScheme).getOrNull()
}

public fun String.toUrlOrThrow(
    sanitizeFragment: Boolean = true,
    setPlusAsBlanks: Boolean = false,
    fallbackScheme: URIScheme? = URIScheme.HTTPS,
): Url {
    return toUrlOrNull(sanitizeFragment, setPlusAsBlanks, fallbackScheme)!!
}
