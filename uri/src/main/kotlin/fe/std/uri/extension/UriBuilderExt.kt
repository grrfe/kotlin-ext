package fe.std.uri.extension

import fe.relocated.org.apache.hc.core5.core5.net.URIBuilder
import fe.std.uri.Url
import fe.std.uri.UrlFactory
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

public fun URIBuilder.toUrl(): Url {
    val uri = build()
    return UrlFactory.createUrl(this, uri)
}

@OptIn(ExperimentalContracts::class)
public inline fun buildUrl(block: URIBuilder.() -> Unit): Url {
    contract {
        callsInPlace(block, InvocationKind.EXACTLY_ONCE)
    }

    return URIBuilder()
        .apply(block)
        .toUrl()
}
