package fe.std.uri

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNull
import assertk.tableOf
import org.junit.Test

internal class UrlFixTest {

    private val invalid = arrayOf("", ":", "http://", "http:/")
    private val valid = tableOf("input", "expected")
        .row("google.com", "https://google.com")
        .row("//google.com", "https://google.com")
        .row("http://google.com", "http://google.com")
        .row("https://google.com", "https://google.com")

    @Test
    fun `http url fixing slow`() {
        invalid.forEach {
            assertThat(UrlFactory.fixHttpUrl(it)).isNull()
        }

        valid.forAll { input, expected ->
            assertThat(UrlFactory.fixHttpUrl(input)).isEqualTo(expected)
        }
    }
}
