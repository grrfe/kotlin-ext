package fe.std.uri

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isInstanceOf
import assertk.assertions.prop
import kotlin.test.Test

internal class UrlTest {
    @Test
    fun test() {
        val result = Url("www.google.com")
        assertThat(result)
            .isInstanceOf<Url>()
            .prop(Url::scheme)
            .isEqualTo("https")
    }
}
